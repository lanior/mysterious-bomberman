package engine

import (
	"errors"
	"fmt"
	"unicode"
)

const (
	MinMapSize = 3
	MaxMapSize = 50
)

type BlockType struct {
	Code         rune
	BlockBombs   bool
	BlockPlayers bool
	KillBombs    bool
	KillPlayers  bool
	Destructable bool
	StopBlast    bool
	Direction    Vector2
}

type ItemType struct {
	Code rune
	Name string
}

type BlockSlice []*BlockType
type ItemSlice []*ItemType

type MapDescription struct {
	Width          int
	Height         int
	Cells          []*CellDescription
	StartPositions []Vector2
}

type CellDescription struct {
	Block    *BlockType
	Next     *CellDescription
	ItemType *ItemType
}

var (
	BlockWall = &BlockType{
		Code:         '#',
		BlockBombs:   true,
		BlockPlayers: true,
		StopBlast:    true,
	}

	BlockDirt = &BlockType{
		Code:         '%',
		BlockBombs:   true,
		BlockPlayers: true,
		Destructable: true,
		StopBlast:    true,
	}

	BlockFloor = &BlockType{Code: '.'}
	BlockLeft  = &BlockType{Code: '<', Direction: Left}
	BlockRight = &BlockType{Code: '>', Direction: Right}
	BlockUp    = &BlockType{Code: '^', Direction: Up}
	BlockDown  = &BlockType{Code: 'v', Direction: Down}

	BlockCollapsar = &BlockType{
		Code:        '*',
		KillBombs:   true,
		KillPlayers: true,
	}

	BlockTypes = BlockSlice{
		BlockWall,
		BlockDirt,
		BlockFloor,
		BlockLeft,
		BlockRight,
		BlockUp,
		BlockDown,
		BlockCollapsar,
	}

	ItemRandom      = &ItemType{}
	ItemBomb        = &ItemType{Code: 'b', Name: "bomb"}
	ItemFlameUp     = &ItemType{Code: 'f', Name: "flame"}
	ItemGoldenFlame = &ItemType{Code: 'g', Name: "goldenflame"}
	ItemSpeedUp     = &ItemType{Code: 's', Name: "speed"}
	ItemKicker      = &ItemType{Code: 'k', Name: "kicker"}
	ItemThrow       = &ItemType{Code: 't', Name: "throw"}

	ItemInvert     = &ItemType{Code: 'i', Name: "invert"}
	ItemLightSpeed = &ItemType{Code: 'c', Name: "lightspeed"}
	ItemAutoBomb   = &ItemType{Code: 'a', Name: "autobomb"}
	ItemNoBomb     = &ItemType{Code: 'n', Name: "nobomb"}
	ItemTurtle     = &ItemType{Code: 'u', Name: "turtle"}

	Diseases = ItemSlice{
		ItemInvert,
		ItemLightSpeed,
		ItemAutoBomb,
		ItemNoBomb,
		ItemTurtle,
	}

	ItemTypes = append(ItemSlice{
		ItemBomb,
		ItemFlameUp,
		ItemGoldenFlame,
		ItemSpeedUp,
		ItemKicker,
		ItemThrow,
	}, Diseases...)

	ErrGapInPlayerNumbers            = errors.New("player numbers have gaps")
	ErrInvalidDestructableCellFormat = errors.New("invalid destructable cell format")
	ErrInvalidLineFormat             = errors.New("invalid line format")
	ErrInvalidMapSize                = errors.New("invalid map size")
	ErrInvalidPlayerCell             = errors.New("invalid player cell")
	ErrMapNotRectangular             = errors.New("expected rectangular map")
	ErrMapWidthMoreThanMax           = errors.New("map width is too big")
	ErrPlayerPositionRedefinition    = errors.New("player position redefinition")
	ErrTooFewPlayers                 = errors.New("too few players")
)

type UnknownBlockError rune

func (e UnknownBlockError) Error() string {
	return fmt.Sprintf("unknown block type %c", rune(e))
}

func (bs BlockSlice) GetByCode(code rune) *BlockType {
	for _, blockType := range bs {
		if blockType.Code == code {
			return blockType
		}
	}
	return nil
}

func (is ItemSlice) GetByCode(code rune) *ItemType {
	if code == 0 {
		return nil
	}
	for _, item := range is {
		if code == item.Code {
			return item
		}
	}
	return nil
}

func LoadMap(field []string) (*MapDescription, error) {
	i := 0
	playerPositions := make(map[rune]Vector2)
	maxPlayerIndex := rune(-1)

	switch {
	case
		len(field) < MinMapSize,
		len(field) > MaxMapSize,
		len(field[0])/2 < MinMapSize,
		len(field[0])/2 > MaxMapSize:

		return nil, ErrInvalidMapSize
	}

	cells := make([]*CellDescription, len(field)*len([]rune(field[0]))/2)
	width := -1
	for y, row := range field {
		runes := []rune(row)

		if width == -1 {
			width = len(runes)
		} else if width != len(runes) {
			return nil, ErrMapNotRectangular
		}

		if width%2 != 0 {
			return nil, ErrInvalidLineFormat
		}

		for x := 0; x < len(runes); x += 2 {
			cellFirst := runes[x]
			cellSecond := runes[x+1]

			cells[i] = &CellDescription{}
			lowItem := ItemTypes.GetByCode(cellFirst)
			highItem := ItemTypes.GetByCode(unicode.ToLower(cellFirst))

			switch {
			case cellFirst >= '0' && cellFirst <= '9':
				cells[i].Block = BlockTypes.GetByCode(cellSecond)
				if cells[i].Block == nil {
					break
				}

				if cells[i].Block.KillPlayers || cells[i].Block.BlockPlayers {
					return nil, ErrInvalidPlayerCell
				}

				index := cellFirst - '0'
				if _, ok := playerPositions[index]; ok {
					return nil, ErrPlayerPositionRedefinition
				}

				playerPositions[index] = Vector2{X: x / 2 * CellSize, Y: y * CellSize}
				if index > maxPlayerIndex {
					maxPlayerIndex = index
				}

			case lowItem != nil:
				cells[i].ItemType = lowItem
				cells[i].Block = BlockTypes.GetByCode(cellSecond)

			case highItem != nil:
				cells[i].Block = BlockDirt
				cells[i].Next = &CellDescription{
					Block:    BlockTypes.GetByCode(cellSecond),
					ItemType: highItem,
				}

			default:
				cells[i].Block = BlockTypes.GetByCode(cellFirst)
				if cells[i].Block == nil {
					return nil, UnknownBlockError(cellFirst)
				}

				nextBlock := BlockTypes.GetByCode(cellSecond)
				if !cells[i].Block.Destructable {
					if nextBlock != cells[i].Block {
						return nil, ErrInvalidDestructableCellFormat
					}
				} else {
					if cells[i].Block == BlockDirt && nextBlock == BlockDirt {
						cells[i].Next = &CellDescription{
							Block:    BlockFloor,
							ItemType: ItemRandom,
						}
					} else {
						cells[i].Next = &CellDescription{
							Block: nextBlock,
						}
					}
				}
			}

			if cells[i].Block == nil {
				return nil, UnknownBlockError(cellSecond)
			}
			if cells[i].Next != nil && cells[i].Next.Block == nil {
				return nil, UnknownBlockError(cellSecond)
			}

			i++
		}
	}

	if len(playerPositions) < 2 {
		return nil, ErrTooFewPlayers
	}

	if len(playerPositions) != int(maxPlayerIndex+1) {
		return nil, ErrGapInPlayerNumbers
	}

	positions := make([]Vector2, len(playerPositions))
	for i, pos := range playerPositions {
		positions[i] = pos
	}

	return &MapDescription{
		Height:         len(field),
		Width:          width / 2,
		Cells:          cells,
		StartPositions: positions,
	}, nil
}

func (m *Map) Cell(x int, y int) *Cell {
	if x < 0 || x >= m.Description.Width || y < 0 || y >= m.Description.Height {
		return nil
	}
	return &m.Cells[y*m.Description.Width+x]
}

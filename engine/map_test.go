package engine

import (
	"github.com/stretchrcom/testify/assert"
	"testing"
)

func getMapLoadError(mapToLoad []string) error {
	_, err := LoadMap(mapToLoad)
	return err
}

func TestMapParse(t *testing.T) {
	assert.Equal(t, getMapLoadError([]string{".."}), ErrInvalidMapSize)
	assert.Equal(t, getMapLoadError([]string{"..", "..", ".."}), ErrInvalidMapSize)
	assert.Equal(t, getMapLoadError([]string{".......", ".......", "......."}), ErrInvalidLineFormat)
	assert.Equal(t, getMapLoadError([]string{"........", "......", "......"}), ErrMapNotRectangular)
	assert.Equal(t, getMapLoadError([]string{"..0...", "..0...", "......"}), ErrPlayerPositionRedefinition)
	assert.Equal(t, getMapLoadError([]string{"..0...", "......", "......"}), ErrTooFewPlayers)
	assert.Equal(t, getMapLoadError([]string{"..0...", "......", "....2."}), ErrGapInPlayerNumbers)

	assert.Equal(t, getMapLoadError([]string{
		"##%.##",
		"##0.1.",
		"%...q.",
	}), UnkownBlockError('q'))

	assert.Equal(t, getMapLoadError([]string{
		"##%.##",
		"##0.1.",
		"%...Sw",
	}), UnkownBlockError('w'))

	assert.Equal(t, getMapLoadError([]string{
		"##%.##",
		"##0*1*",
		"%...S.",
	}), ErrInvalidPlayerCell)

	assert.Equal(t, getMapLoadError([]string{
		"##%.##",
		"##0%1.",
		"%...S.",
	}), ErrInvalidPlayerCell)

	assert.Equal(t, getMapLoadError([]string{
		"##%.##",
		"#*0%1.",
		"%...S.",
	}), ErrInvalidInvulnerableCellFormat)

	assert.Equal(t, getMapLoadError([]string{
		"b.s.f.g.k.t.",
		"i.c.a.n.u.0.",
		"B.S.F.G.K.T.",
		"I.C.A.N.U.1.",
		"%*%>%<%^%v%.",
	}), nil)

	descr, err := LoadMap([]string{
		"##%>0v",
		"b*B<<<",
		"1.....",
	})
	assert.Equal(t, err, nil)
	assert.True(t, descr.Height == 3 && descr.Width == 3)

	pos := make([]Vector2, 2)
	pos[0] = Vector2{2 * CellSize, 0}
	pos[1] = Vector2{0, 2 * CellSize}
	assert.Equal(t, descr.StartPositions, pos)

	assert.Equal(t, *descr.Cells[0], CellDescription{Block: BlockWall})
	assert.Equal(t, *descr.Cells[1], CellDescription{Block: BlockDirt, Next: &CellDescription{Block: BlockRight}})
	assert.Equal(t, *descr.Cells[2], CellDescription{Block: BlockDown})
	assert.Equal(t, *descr.Cells[3], CellDescription{Block: BlockCollapsar, ItemType: ItemBomb})
	assert.Equal(t, *descr.Cells[4], CellDescription{
		Block: BlockDirt,
		Next: &CellDescription{
			Block:    BlockLeft,
			ItemType: ItemBomb,
		},
	})
	assert.Equal(t, *descr.Cells[5], CellDescription{Block: BlockLeft})
	assert.Equal(t, *descr.Cells[6], CellDescription{Block: BlockFloor})
	assert.Equal(t, *descr.Cells[7], CellDescription{Block: BlockFloor})
	assert.Equal(t, *descr.Cells[8], CellDescription{Block: BlockFloor})
}

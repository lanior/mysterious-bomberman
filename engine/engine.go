package engine

import (
	"errors"
	"math/rand"
	"sync"
	"time"
)

const (
	CellSize = 10000

	TicksPerSecond = 30
	TickPeriod     = 1000 / TicksPerSecond * time.Millisecond
	EndGamePeriod  = TicksPerSecond
)

type GameConfiguration struct {
	RandomSeed      uint32
	MaxSlideLength  int
	PlayerBaseSpeed int

	BombSpeed              int
	BombDelay              int
	BombChainReactionDelay int

	BlastWaveDuration   int
	CellDecayDuration   int
	ItemDropProbability int

	Items ItemsConfigurationMap
}

type ItemConfiguration struct {
	cumulativeWeight int

	Weight     int
	StartCount int
	MaxCount   int

	Speed    int
	Duration int

	SpeedBonus int
	FlameBonus int
	BombBonus  int

	AutoBomb       bool
	NoBomb         bool
	InvertMovement bool

	Group      int
	ClearGroup int
}

type ItemsConfigurationMap map[*ItemType]*ItemConfiguration

type Engine struct {
	players            []Player
	pendingActions     []pendingAction
	pendingActionsLock sync.RWMutex
	random             *MersenneTwister
	config             *GameConfiguration
	totalItemWeight    int
	endGameAt          int
	blockActions       bool

	State *GameState
}

type pendingAction struct {
	MovementDirection Vector2

	PlaceBomb   bool
	ResetAction bool
}

type GameState struct {
	Tick            int
	CalculationTime int64

	Map        Map
	Players    []*GamePlayerState
	Bombs      []*Bomb
	Explosions []*Explosion
}

type Bomb struct {
	Pos        Vector2
	Owner      Player
	CreatedAt  int
	ExplodesAt int

	BlastRadius       int
	Speed             int
	MovementDirection Vector2
}

type Explosion struct {
	Pos       Vector2
	CreatedAt int
	DestroyAt int

	Up    int
	Down  int
	Left  int
	Right int
}

type Player int

type GamePlayerState struct {
	PendingFireActions int
	MovementDirection  Vector2
	DirectionChangedAt int
	LookDirection      Vector2

	Pos  Vector2
	Dead bool

	PlacedBombs int

	PermamentItems map[*ItemType]int
	TemporaryItems []*TemporaryItem

	Speed          int
	MaxBombs       int
	BlastRadius    int
	AutoBomb       bool
	InvertMovement bool
}

type Map struct {
	Description *MapDescription
	Cells       []Cell
}

type Cell struct {
	Block    *BlockType
	Next     *Cell
	ItemType *ItemType

	Pos   Vector2
	Index Vector2

	DestroyAt       int
	Bomb            *Bomb
	Players         []*GamePlayerState
	ExplosionsCount int
}

type TemporaryItem struct {
	ItemType *ItemType
	Config   *ItemConfiguration
	RemoveAt int
}

var (
	None  = Vector2{0, 0}
	Left  = Vector2{-1, 0}
	Right = Vector2{1, 0}
	Up    = Vector2{0, -1}
	Down  = Vector2{0, 1}

	cellPerSecond = CellSize / TicksPerSecond

	DefaultGameConfiguration = &GameConfiguration{
		RandomSeed: 874654654,

		MaxSlideLength:      5000,
		ItemDropProbability: 55,
		PlayerBaseSpeed:     2*cellPerSecond + cellPerSecond/4,

		BombSpeed:              4 * cellPerSecond,
		BombDelay:              int(time.Second * 5 / 2 / TickPeriod),
		BombChainReactionDelay: int(time.Second / 10 / TickPeriod),

		BlastWaveDuration: int(time.Second / 2 / TickPeriod),
		CellDecayDuration: int(time.Second / 2 / TickPeriod),

		Items: ItemsConfigurationMap{
			ItemBomb: &ItemConfiguration{
				Weight:     18,
				StartCount: 1,
				MaxCount:   9,
				BombBonus:  1,
			},

			ItemSpeedUp: &ItemConfiguration{
				Weight:     18,
				StartCount: 0,
				MaxCount:   5,
				SpeedBonus: cellPerSecond,
			},

			ItemFlameUp: &ItemConfiguration{
				Weight:     18,
				StartCount: 1,
				MaxCount:   12,
				FlameBonus: 1,
			},

			ItemGoldenFlame: &ItemConfiguration{
				Weight:     2,
				MaxCount:   1,
				FlameBonus: 100,
			},

			ItemKicker: &ItemConfiguration{Weight: 0, MaxCount: 1},
			ItemThrow:  &ItemConfiguration{Weight: 0, MaxCount: 1},

			ItemInvert: &ItemConfiguration{
				Weight:         4,
				Duration:       10 * TicksPerSecond,
				InvertMovement: true,

				Group:      1,
				ClearGroup: 1,
			},

			ItemLightSpeed: &ItemConfiguration{
				Weight:   4,
				Duration: 10 * TicksPerSecond,
				Speed:    15 * cellPerSecond,

				Group:      1,
				ClearGroup: 1,
			},

			ItemAutoBomb: &ItemConfiguration{
				Weight:   4,
				Duration: 10 * TicksPerSecond,
				AutoBomb: true,

				Group:      1,
				ClearGroup: 1,
			},

			ItemNoBomb: &ItemConfiguration{
				Weight:   4,
				Duration: 10 * TicksPerSecond,
				NoBomb:   true,

				Group:      1,
				ClearGroup: 1,
			},

			ItemTurtle: &ItemConfiguration{
				Weight:   4,
				Duration: 10 * TicksPerSecond,
				Speed:    cellPerSecond,

				Group:      1,
				ClearGroup: 1,
			},
		},
	}

	ErrInvalidMovementAction = errors.New("invalid movement action")
	ErrWrongTick             = errors.New("wrong tick")
	ErrTooManyPlayers        = errors.New("too many players")
)

func (ic *ItemConfiguration) Temporary() bool {
	return ic.Duration > 0
}

func NewEngine(config *GameConfiguration, mapDescription *MapDescription, players []Player) (*Engine, error) {
	if config == nil {
		config = DefaultGameConfiguration
	}

	engine := &Engine{
		players:        players,
		pendingActions: make([]pendingAction, len(players)),
		random:         NewMersenneTwister(config.RandomSeed),
		config:         config,
		endGameAt:      -1,
	}

	if len(players) > len(mapDescription.StartPositions) {
		return nil, ErrTooManyPlayers
	}

	cumulativeWeight := 0
	for _, item := range ItemTypes {
		itemConfig := engine.config.Items[item]
		cumulativeWeight += itemConfig.Weight
		itemConfig.cumulativeWeight = cumulativeWeight
	}
	engine.totalItemWeight = cumulativeWeight
	engine.computeInitialState(mapDescription, players)
	return engine, nil
}

func (g *Engine) Tick() {
	t1 := time.Now()
	g.State.Tick++
	g.destroyDecayedObjects()
	g.detonateBombs()
	g.playerActions()
	g.State.CalculationTime = time.Now().Sub(t1).Nanoseconds()
}

func (g *Engine) objectCell(pos Vector2) *Cell {
	p := Vector2{
		X: (pos.X + CellSize/2) / CellSize,
		Y: (pos.Y + CellSize/2) / CellSize,
	}
	return g.cellByIndex(p)
}

func (g *Engine) cellByIndex(pos Vector2) *Cell {
	cell := g.State.Map.Cell(pos.X, pos.Y)
	if cell == nil {
		return &Cell{
			Block:     BlockWall,
			Pos:       pos.ScaleBy(CellSize),
			Index:     pos,
			DestroyAt: -1,
		}
	}
	return cell
}

func (g *Engine) getRandomItem() *ItemType {
	n := g.random.Next() % 100
	if n < uint32(g.config.ItemDropProbability) {
		if g.totalItemWeight == 0 {
			return nil
		}

		r := g.random.Next() % uint32(g.totalItemWeight)
		for _, item := range ItemTypes {
			if r < uint32(g.config.Items[item].cumulativeWeight) {
				return item
			}
		}
	}
	return nil
}

func (g *Engine) destroyDecayedObjects() {
	cells := g.State.Map.Cells
	for i, cell := range cells {
		if cell.DestroyAt == g.State.Tick {
			newCell := cell.Next
			newCell.Pos = cell.Pos
			newCell.Index = cell.Index
			newCell.Players = cell.Players
			newCell.ExplosionsCount = cell.ExplosionsCount
			if cell.Bomb != nil {
				newCell.Bomb = cell.Bomb
			}
			if newCell.ItemType == ItemRandom {
				newCell.ItemType = g.getRandomItem()
			}
			cells[i] = *newCell
		}
	}

	explosions := g.State.Explosions
	size := len(explosions)
	for i := 0; i < size; i++ {
		explosion := explosions[i]
		if explosion.DestroyAt == g.State.Tick {
			cell := g.objectCell(explosion.Pos)
			cell.ExplosionsCount--
			g.destroyBlastWave(cell, Up, explosion.Up)
			g.destroyBlastWave(cell, Down, explosion.Down)
			g.destroyBlastWave(cell, Left, explosion.Left)
			g.destroyBlastWave(cell, Right, explosion.Right)

			explosions[i] = explosions[size-1]
			explosions = explosions[:size-1]
			size--
			i--
		}
	}
	g.State.Explosions = explosions
}

func (g *Engine) killPlayer(player *GamePlayerState) {
	player.Dead = true
	player.Speed = 0
	player.MovementDirection = None
}

func (g *Engine) blowUpCell(cell *Cell) bool {
	protected := false
	if cell.ItemType != nil {
		cell.ItemType = nil
		protected = true
	}

	if cell.Bomb != nil {
		explodesAt := g.State.Tick + g.config.BombChainReactionDelay
		if explodesAt < cell.Bomb.ExplodesAt {
			cell.Bomb.ExplodesAt = explodesAt
		}
		protected = true
	}

	for _, player := range cell.Players {
		if !player.Dead {
			g.killPlayer(player)
			protected = true
		}
	}

	if !protected && cell.Block.Destructable {
		destroyAt := g.State.Tick + g.config.CellDecayDuration
		if cell.DestroyAt == -1 || cell.DestroyAt < destroyAt {
			cell.DestroyAt = destroyAt
		}
	}
	return !protected && !cell.Block.StopBlast
}

func (g *Engine) sendBlastWave(cell *Cell, dir Vector2, radius int) int {
	length := 0
	for i := 1; i <= radius; i++ {
		currentCell := g.cellByIndex(cell.Index.Add(dir.ScaleBy(i)))
		if !g.blowUpCell(currentCell) {
			break
		}
		currentCell.ExplosionsCount++
		length++
	}
	return length
}

func (g *Engine) destroyBlastWave(cell *Cell, dir Vector2, length int) {
	for i := 1; i <= length; i++ {
		g.cellByIndex(cell.Index.Add(dir.ScaleBy(i))).ExplosionsCount--
	}
}

func (g *Engine) detonateBombs() {
	j := 0
	bombs := g.State.Bombs
	for _, bomb := range bombs {
		if g.State.Tick == bomb.ExplodesAt {
			cell := g.objectCell(bomb.Pos)
			cell.Bomb = nil
			cell.ExplosionsCount++

			g.blowUpCell(cell)
			g.State.Players[bomb.Owner].PlacedBombs--

			g.State.Explosions = append(g.State.Explosions, &Explosion{
				Pos:       cell.Pos,
				CreatedAt: g.State.Tick,
				DestroyAt: g.State.Tick + g.config.BlastWaveDuration,

				Up:    g.sendBlastWave(cell, Up, bomb.BlastRadius),
				Down:  g.sendBlastWave(cell, Down, bomb.BlastRadius),
				Left:  g.sendBlastWave(cell, Left, bomb.BlastRadius),
				Right: g.sendBlastWave(cell, Right, bomb.BlastRadius),
			})
		} else {
			bombs[j] = bomb
			j++
		}
	}
	g.State.Bombs = bombs[:j]
}

func (g *Engine) getPlayerObstacles(player *GamePlayerState) (result []*Cell) {
	x1 := player.Pos.X / CellSize
	y1 := player.Pos.Y / CellSize
	x2 := (player.Pos.X + CellSize - 1) / CellSize
	y2 := (player.Pos.Y + CellSize - 1) / CellSize

	if player.Pos.X < 0 {
		x1 = -1
	}

	if player.Pos.Y < 0 {
		y1 = -1
	}

	for x := x1; x <= x2; x++ {
		for y := y1; y <= y2; y++ {
			cell := g.cellByIndex(Vector2{x, y})
			if cell.Block.BlockPlayers || cell.Bomb != nil {
				result = append(result, cell)
			}
		}
	}
	return
}

func (g *Engine) intersects(a Vector2, b Vector2) bool {
	if max(a.X, b.X) <= min(a.X, b.X)+CellSize-1 {
		if max(a.Y, b.Y) <= min(a.Y, b.Y)+CellSize-1 {
			return true
		}
	}
	return false
}

func (g *Engine) movePlayer(player *GamePlayerState, distance int, directionVector Vector2) ([]*Cell, int) {
	if player.Dead || distance <= 0 {
		return nil, 0
	}

	oldCell := g.objectCell(player.Pos)
	oldPos := player.Pos
	oldObstacles := g.getPlayerObstacles(player)

	player.Pos = player.Pos.Add(directionVector.ScaleBy(distance))

	var obstacles []*Cell
	for _, cell := range g.getPlayerObstacles(player) {
		ok := true
		for _, oldCell := range oldObstacles {
			if oldCell == cell {
				ok = false
				break
			}
		}
		if ok {
			obstacles = append(obstacles, cell)
		}
	}

	if len(obstacles) > 0 {
		switch player.MovementDirection {
		case Up:
			player.Pos.Y = obstacles[0].Pos.Y + CellSize
		case Down:
			player.Pos.Y = obstacles[0].Pos.Y - CellSize
		case Left:
			player.Pos.X = obstacles[0].Pos.X + CellSize
		case Right:
			player.Pos.X = obstacles[0].Pos.X - CellSize
		}
	}

	cell := g.objectCell(player.Pos)
	if oldCell != cell {
		for i, cellPlayer := range oldCell.Players {
			if cellPlayer == player {
				oldCell.Players[i] = oldCell.Players[len(oldCell.Players)-1]
				oldCell.Players = oldCell.Players[:len(oldCell.Players)-1]
				break
			}
		}
		cell.Players = append(cell.Players, player)

		if cell.Block.KillPlayers {
			g.killPlayer(player)
		}
	}
	return obstacles, distance - abs(player.Pos.X-oldPos.X) - abs(player.Pos.Y-oldPos.Y)
}

func intersectionLen(a, b Vector2) int {
	return max(0, CellSize-abs(a.X-b.X)) + max(0, CellSize-abs(a.Y-b.Y))
}

func removeItems(items []*TemporaryItem, pred func(*TemporaryItem) bool) ([]*TemporaryItem, bool) {
	j := 0
	updated := false
	for i, ti := range items {
		if pred(ti) {
			updated = true
		} else {
			items[j] = items[i]
			j++
		}
	}
	return items[:j], updated
}

func (g *Engine) removeOldItems(player *GamePlayerState) {
	items, updated := removeItems(player.TemporaryItems, func(ti *TemporaryItem) bool {
		return g.State.Tick >= ti.RemoveAt
	})

	player.TemporaryItems = items
	if updated {
		g.calculatePlayerStats(player)
	}
}

func (g *Engine) playerActions() {
	g.pendingActionsLock.RLock()
	for i, player := range g.State.Players {
		if player.Dead {
			continue
		}

		g.removeOldItems(player)

		newDirection := g.pendingActions[i].MovementDirection
		if player.InvertMovement {
			newDirection = newDirection.ScaleBy(-1)
		}

		if newDirection != player.MovementDirection {
			player.DirectionChangedAt = g.State.Tick
		}
		player.MovementDirection = newDirection

		if player.MovementDirection != None {
			player.LookDirection = player.MovementDirection
		}

		cell := g.objectCell(player.Pos)
		if cell.ExplosionsCount > 0 {
			g.killPlayer(player)
			continue
		}

		if g.pendingActions[i].PlaceBomb || player.AutoBomb {
			if cell.Bomb == nil && player.PlacedBombs < player.MaxBombs {
				bomb := &Bomb{
					Pos:         cell.Pos,
					Owner:       Player(i),
					CreatedAt:   g.State.Tick,
					ExplodesAt:  g.State.Tick + g.config.BombDelay,
					BlastRadius: player.BlastRadius,
					Speed:       g.config.BombSpeed,
				}
				g.State.Bombs = append(g.State.Bombs, bomb)
				player.PlacedBombs++
				cell.Bomb = bomb
			}
		}

		if g.pendingActions[i].ResetAction {
			g.pendingActions[i].PlaceBomb = false
			g.pendingActions[i].ResetAction = false
		}

		remainDistance := player.Speed
		directionVector := player.MovementDirection

		obstacles, remainDistance := g.movePlayer(player, remainDistance, directionVector)
		if remainDistance > 0 && len(obstacles) == 1 {
			slideVector := player.Pos.Sub(obstacles[0].Pos).Sign().Add(directionVector)

			intersectionLength := intersectionLen(player.Pos, obstacles[0].Pos)
			if intersectionLength <= g.config.MaxSlideLength {
				slideLen := min(intersectionLength, remainDistance)
				g.movePlayer(player, slideLen, slideVector)
				g.movePlayer(player, remainDistance-slideLen, directionVector)
			}
		}

		cell = g.objectCell(player.Pos)
		if cell.ExplosionsCount > 0 {
			g.killPlayer(player)
		} else if cell.ItemType != nil {
			config := g.config.Items[cell.ItemType]
			if config.Temporary() {
				if config.ClearGroup > 0 {
					player.TemporaryItems, _ = removeItems(player.TemporaryItems, func(ti *TemporaryItem) bool {
						return ti.Config.Group == config.ClearGroup
					})
				}

				player.TemporaryItems = append(player.TemporaryItems, &TemporaryItem{
					ItemType: cell.ItemType,
					Config:   config,
					RemoveAt: g.State.Tick + config.Duration,
				})
			} else {
				player.PermamentItems[cell.ItemType] = min(config.MaxCount, player.PermamentItems[cell.ItemType]+1)
			}
			g.calculatePlayerStats(player)
			cell.ItemType = nil
		}
	}
	g.pendingActionsLock.RUnlock()
}

func (g *Engine) SetMovementDirection(p Player, dir Vector2) {
	g.pendingActionsLock.Lock()
	g.pendingActions[p].MovementDirection = dir
	g.pendingActionsLock.Unlock()
}

func (g *Engine) PlaceBomb(p Player) {
	g.pendingActionsLock.Lock()
	g.pendingActions[p].PlaceBomb = true
	g.pendingActionsLock.Unlock()
}

func (g *Engine) ResetAction(p Player) {
	g.pendingActionsLock.Lock()
	g.pendingActions[p].ResetAction = true
	g.pendingActionsLock.Unlock()
}

func (g *Engine) GameEnd() bool {
	if g.endGameAt == -1 {
		count := 0
		for _, player := range g.State.Players {
			if !player.Dead {
				count++
			}
		}

		if count <= 1 {
			g.endGameAt = g.State.Tick
			g.blockActions = true
		}
		return false
	}

	if len(g.State.Bombs) == 0 && len(g.State.Explosions) > 0 {
		return false
	}
	return g.State.Tick >= g.endGameAt+EndGamePeriod
}

func (g *Engine) Winner() Player {
	for i, player := range g.State.Players {
		if !player.Dead {
			return Player(i)
		}
	}
	return -1
}

func (g *Engine) calculatePlayerStats(player *GamePlayerState) {
	player.Speed = g.config.PlayerBaseSpeed
	player.MaxBombs = 0
	player.BlastRadius = 0
	player.AutoBomb = false
	player.InvertMovement = false

	applyItem := func(item *ItemConfiguration, amount int) {
		player.Speed += amount * item.SpeedBonus
		player.MaxBombs += amount * item.BombBonus
		player.BlastRadius += amount * item.FlameBonus

		if item.Speed > 0 {
			player.Speed = item.Speed
		}

		if item.InvertMovement {
			player.InvertMovement = true
		}

		if item.AutoBomb {
			player.AutoBomb = true
		}

		if item.NoBomb {
			player.MaxBombs = 0
		}
	}

	for item, amount := range player.PermamentItems {
		applyItem(g.config.Items[item], amount)
	}

	for _, ti := range player.TemporaryItems {
		applyItem(g.config.Items[ti.ItemType], 1)
	}
}

func (g *Engine) computeInitialState(mapDescription *MapDescription, players []Player) {
	g.State = &GameState{}
	g.State.Players = make([]*GamePlayerState, len(players))
	g.State.Map.Description = mapDescription
	g.State.Map.Cells = make([]Cell, len(mapDescription.Cells))
	for i, cell := range mapDescription.Cells {
		var next *Cell
		index := Vector2{
			X: (i % mapDescription.Width),
			Y: (i / mapDescription.Width),
		}
		pos := Vector2{
			X: index.X * CellSize,
			Y: index.Y * CellSize,
		}
		if cell.Next != nil {
			next = &Cell{
				Block:     cell.Next.Block,
				ItemType:  cell.Next.ItemType,
				DestroyAt: -1,
			}
		}
		g.State.Map.Cells[i] = Cell{
			Block:     cell.Block,
			ItemType:  cell.ItemType,
			Next:      next,
			Pos:       pos,
			Index:     index,
			DestroyAt: -1,
		}
	}

	perm := rand.Perm(len(mapDescription.StartPositions))
	for i := range players {
		p := &GamePlayerState{
			Pos:               mapDescription.StartPositions[perm[i]],
			MovementDirection: None,
			LookDirection:     Down,
			PermamentItems:    make(map[*ItemType]int),
		}

		for item, config := range g.config.Items {
			if config.Temporary() {
				if config.StartCount > 0 {
					p.TemporaryItems = append(p.TemporaryItems, &TemporaryItem{
						ItemType: item,
						Config:   config,
						RemoveAt: config.Duration,
					})
				}
			} else {
				p.PermamentItems[item] = config.StartCount
			}
		}
		g.calculatePlayerStats(p)
		g.State.Players[i] = p

		cell := g.objectCell(g.State.Players[i].Pos)
		cell.Players = append(cell.Players, g.State.Players[i])
	}
}

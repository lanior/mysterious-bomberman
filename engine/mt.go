package engine

// MT19937 (http://en.wikipedia.org/wiki/Mersenne_twister)
type MersenneTwister struct {
	numbers [624]uint32
	index   int
}

func NewMersenneTwister(seed uint32) *MersenneTwister {
	mt := &MersenneTwister{}
	mt.numbers[0] = seed
	for i := uint32(1); i < 624; i++ {
		mt.numbers[i] = 0x6c078965*(mt.numbers[i-1]^(mt.numbers[i-1]>>30)) + i
	}
	return mt
}

func (mt *MersenneTwister) Next() uint32 {
	if mt.index == 0 {
		for i := 0; i < 624; i++ {
			y := (mt.numbers[i] & 0x80000000) + (mt.numbers[(i+1)%624] & 0x7fffffff)
			mt.numbers[i] = mt.numbers[(i+397)%624] ^ (y >> 1)
			if y%2 != 0 {
				mt.numbers[i] = mt.numbers[i] ^ 0x9908b0df
			}
		}
	}

	y := mt.numbers[mt.index]
	y ^= y >> 11
	y ^= (y << 7) & 0x9d2c5680
	y ^= (y << 15) & 0xefc60000
	y ^= y >> 18

	mt.index = (mt.index + 1) % 624
	return y
}

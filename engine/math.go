package engine

type Vector2 struct {
	X int
	Y int
}

func (v Vector2) ScaleBy(mult int) Vector2 {
	return Vector2{X: v.X * mult, Y: v.Y * mult}
}

func (v Vector2) Add(v2 Vector2) Vector2 {
	return Vector2{X: v.X + v2.X, Y: v.Y + v2.Y}
}

func (v Vector2) Sub(v2 Vector2) Vector2 {
	return Vector2{X: v.X - v2.X, Y: v.Y - v2.Y}
}

func (v Vector2) Sign() Vector2 {
	return Vector2{X: sign(v.X), Y: sign(v.Y)}
}

func (v Vector2) Perpendicular() Vector2 {
	return Vector2{X: -v.Y, Y: v.X}
}

func max(a int, b int) int {
	if a > b {
		return a
	}
	return b
}

func min(a int, b int) int {
	if a < b {
		return a
	}
	return b
}

func abs(x int) int {
	if x < 0 {
		return -x
	}
	return x
}

func sign(x int) int {
	if x > 0 {
		return 1
	} else if x < 0 {
		return -1
	}
	return 0
}

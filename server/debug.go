package server

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

type Reset struct{}

type GameSetDebugMode struct {
	RandomSeed int
}

type GameSetTick struct {
	Tick int
}

type GameSetTickResponse struct {
	GameState *WSGameState
	GameEnded bool
	Winner    string
}

type DebugGameAction struct {
	GameAction WSGameAction `field:"game_action"`
}

func (server *Server) Reset(request *Reset) (Response, error) {
	if !server.debug {
		return nil, ErrUnsupportedAction
	}
	collections := []*mgo.Collection{server.users, server.games, server.counters, server.maps}
	for _, c := range collections {
		if _, err := c.RemoveAll(bson.M{}); err != nil {
			return nil, err
		}
	}

	server.connLock.Lock()
	server.enginesLock.Lock()
	server.gameIdsLock.Lock()

	server.conns = make(map[PersistentConn]bson.ObjectId)
	server.connsByUserId = make(map[bson.ObjectId]PersistentConn)
	server.enginesByUserId = make(map[bson.ObjectId]*engineDescriptor)
	server.gameIdByUserId = make(map[bson.ObjectId]bson.ObjectId)

	server.gameIdsLock.Unlock()
	server.enginesLock.Unlock()
	server.connLock.Unlock()
	return nil, StatusOk
}

func (s *Server) GameSetDebugMode(request *GameSetDebugMode, user *User, game *Game) (Response, error) {
	if request.RandomSeed <= 0 {
		return nil, ErrMalformedRequest
	}

	if game.HasStarted() {
		return nil, ErrGameAlreadyStarted
	}

	q := bson.M{"$set": bson.M{
		"debug_mode":  true,
		"random_seed": request.RandomSeed,
	}}
	if err := s.games.UpdateId(game.Id, q); err != nil {
		return nil, err
	}
	return nil, StatusOk
}

func (s *Server) GameSetTick(request *GameSetTick, user *User, game *Game) (Response, error) {
	if !game.HasStarted() {
		return nil, ErrMalformedRequest
	}

	s.enginesLock.Lock()
	descriptor, ok := s.enginesByUserId[user.Id]
	s.enginesLock.Unlock()

	if !ok || !descriptor.DebugMode {
		return nil, ErrMalformedRequest
	}

	descriptor.DebugModeLock.Lock()
	defer descriptor.DebugModeLock.Unlock()

	gameEngine := descriptor.Engine
	if request.Tick < gameEngine.State.Tick {
		return nil, ErrMalformedRequest
	}

	for i := gameEngine.State.Tick; i < request.Tick; i++ {
		gameEngine.Tick()
	}

	gameEnd := gameEngine.GameEnd()
	winner := ""
	if gameEnd {
		winnerPos := gameEngine.Winner()
		if winnerPos != -1 {
			winner = game.Users[winnerPos].Username
		}
	}
	return &GameSetTickResponse{
		GameState: s.prepareGameStateMessage(gameEngine.State),
		GameEnded: gameEnd,
		Winner:    winner,
	}, nil
}

func (s *Server) DebugGameAction(request *DebugGameAction, user *User, game *Game) (Response, error) {
	if !game.DebugMode || !game.HasStarted() {
		return nil, ErrMalformedRequest
	}
	s.applyGameAction(user.Id, request.GameAction.Direction, request.GameAction.Action)
	return nil, StatusOk
}

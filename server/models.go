package server

import (
	"bytes"
	"crypto/rand"
	"crypto/sha1"
	"encoding/hex"
	"labix.org/v2/mgo/bson"
)

type User struct {
	Id           bson.ObjectId `bson:"_id,omitempty"`
	Username     string
	Password     Password
	Sid          string        `bson:"sid,omitempty"`
	GameId       bson.ObjectId `bson:"game_id,omitempty"`
	ReadyToStart bool          `bson:"ready_to_start,omitempty"`
}

type Game struct {
	Id           bson.ObjectId `bson:"_id,omitempty"`
	PublicId     int           `bson:"public_id"`
	Password     Password
	Name         string
	TotalSlots   int         `bson:"total_slots"`
	FreeSlots    int         `bson:"free_slots"`
	ReadyToStart int         `bson:"ready_to_start"`
	Users        []*GameUser `bson:"users,omitempty"`
	Messages     []*Message  `bson:"messages,omitempty"`
	MapPublicId  int         `bson:"map_public_id"`
	Settings     GameSettings

	DebugMode  bool   `bson:"debug_mode,omitempty"`
	RandomSeed uint32 `bson:"random_seed,omitempty"`
}

type GameUser struct {
	UserId       bson.ObjectId `bson:"user_id"`
	Username     string
	ReadyToStart bool `bson:"ready_to_start"`
}

type Message struct {
	UserId    bson.ObjectId `bson:"user_id"`
	Username  string
	Message   string
	Timestamp int64
}

type Password struct {
	Salt []byte
	Hash []byte
}

type Map struct {
	Id             bson.ObjectId `bson:"_id,omitempty"`
	PublicId       int           `bson:"public_id"`
	Name           string        `bson:"name"`
	Field          []string      `bson:"field"`
	MaxPlayerCount int           `bson:"max_player_count"`
	Preloaded      bool          `bson:"preloaded"`
	Settings       GameSettings  `bson:"settings"`
}

type GameSettings struct {
	MaxSlideLength         int
	PlayerBaseSpeed        int
	BombSpeed              int
	BombDelay              int
	BombChainReactionDelay int
	BlastWaveDuration      int
	CellDecayDuration      int
	ItemDropProbability    int
	Items                  map[string]ItemDescription
}

type ItemDescription struct {
	Weight     int `bson:"weight"`
	StartCount int `bson:"start_count"`
	MaxCount   int `bson:"max_count"`

	Speed      int `bson:"speed,omitempty"`
	Duration   int `bson:"duration,omitempty"`
	SpeedBonus int `bson:"speed_bonus,omitempty"`
	FlameBonus int `bson:"flame_bonus,omitempty"`
	BombBonus  int `bson:"bomb_bonus,omitempty"`
}

func generateSid() (string, error) {
	sid := make([]byte, 16)
	if _, err := rand.Read(sid); err != nil {
		return "", err
	}
	return hex.EncodeToString(sid), nil
}

func (g *Game) HasStarted() bool {
	return g.ReadyToStart >= 2 && g.ReadyToStart >= g.TotalSlots-g.FreeSlots
}

func (p *Password) calcHash(password string, salt []byte) []byte {
	hash := sha1.New()
	hash.Write([]byte(password))
	hash.Write(salt)
	return hash.Sum(nil)
}

func (p *Password) Set(password string) error {
	p.Salt = make([]byte, 8)
	if _, err := rand.Read(p.Salt); err != nil {
		return err
	}
	p.Hash = p.calcHash(password, p.Salt)
	return nil
}

func (p *Password) EqualsTo(password string) bool {
	return bytes.Compare(p.calcHash(password, p.Salt), p.Hash) == 0
}

package server

import (
	"github.com/lanior/mysterious-bomberman/engine"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

var (
	ErrMapNotFound = ResponseStatus("map_not_found")
)

type GetMap struct {
	MapId int
}

type GetMapResponse struct {
	Id             int
	Name           string
	Field          []string
	MaxPlayerCount int
	Settings       GameSettings
}

type UploadMap struct {
	Name     string `format:".{1,32}"`
	Field    []string
	Settings GameSettings
}

type UploadMapResponse struct {
	MapId int
}

type ListMaps struct{}

type ListMapsResponse struct {
	Maps []BriefMap
}

type BriefMap struct {
	MapId int
	Name  string `format:.{1,32}`
}

func (s *Server) GetMap(request *GetMap, user *User) (Response, error) {
	var gameMap Map
	if err := s.maps.Find(bson.M{"public_id": request.MapId}).One(&gameMap); err != nil {
		if err == mgo.ErrNotFound {
			return nil, ErrMapNotFound
		}
		return nil, err
	}
	return &GetMapResponse{
		Id:             gameMap.PublicId,
		Name:           gameMap.Name,
		Field:          gameMap.Field,
		MaxPlayerCount: gameMap.MaxPlayerCount,
		Settings:       gameMap.Settings,
	}, nil
}

func (s *Server) uploadMap(request *UploadMap, preloaded bool) (int, error) {
	if err := ValidateGameSettings(&request.Settings); err != nil {
		return 0, err
	}

	descr, err := engine.LoadMap(request.Field)
	if err != nil {
		return 0, err
	}

	publicId, err := s.nextCounterValue("Maps")
	if err != nil {
		return 0, err
	}

	gameMap := &Map{
		Id:             bson.NewObjectId(),
		PublicId:       publicId,
		Name:           request.Name,
		Field:          request.Field,
		MaxPlayerCount: len(descr.StartPositions),
		Settings:       request.Settings,
		Preloaded:      preloaded,
	}

	if err := s.maps.Insert(gameMap); err != nil {
		return 0, err
	}

	return publicId, nil
}

func (s *Server) UploadMap(request *UploadMap, user *User) (Response, error) {
	publicId, err := s.uploadMap(request, false)
	if err != nil {
		return nil, err
	}
	return &UploadMapResponse{MapId: publicId}, nil
}

func (s *Server) ListMaps(request *ListMaps, user *User) (Response, error) {
	var gameMap Map
	var maps []BriefMap
	iter := s.maps.Find(nil).Iter()
	for iter.Next(&gameMap) {
		maps = append(maps, BriefMap{
			MapId: gameMap.PublicId,
			Name:  gameMap.Name,
		})
	}
	if iter.Err() != nil {
		return nil, iter.Err()
	}
	return &ListMapsResponse{Maps: maps}, nil
}

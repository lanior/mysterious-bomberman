package server

import (
	"code.google.com/p/go.net/websocket"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"net/url"
	"strings"
	"syscall"
)

type httpHandler struct {
	server           *Server
	webSocketHandler http.Handler
}

type webSocketConn struct {
	ws *websocket.Conn
}

func (c *webSocketConn) Send(message map[string]interface{}) error {
	return websocket.JSON.Send(c.ws, message)
}

func (c *webSocketConn) Close() error {
	return c.ws.Close()
}

func newHttpHandler(server *Server) *httpHandler {
	return &httpHandler{
		server: server,
		webSocketHandler: websocket.Handler(func(ws *websocket.Conn) {
			conn := &webSocketConn{ws: ws}
			server.RegisterUserConn(conn)
			for {
				var message map[string]interface{}
				for {
					if err := websocket.JSON.Receive(ws, &message); err != nil {
						if err2, ok := err.(*net.OpError); ok {
							if err2.Err == syscall.EAGAIN {
								continue
							}
						}
						server.CloseUserConn(conn)
						return
					}
					break
				}
				server.DispatchUserMessage(conn, message)
			}
		}),
	}
}

func (h *httpHandler) getQueryField(values url.Values, name string) string {
	fields := values[name]
	if len(fields) == 0 {
		return ""
	}
	return fields[len(fields)-1]
}

func (h *httpHandler) dispatch(request []byte, host string) map[string]interface{} {
	r := make(map[string]interface{})
	if err := json.Unmarshal(request, &r); err != nil {
		return map[string]interface{}{"status": ErrMalformedRequest}
	}
	return h.server.Dispatch(r, host)
}

func (h *httpHandler) serveApiRequest(w http.ResponseWriter, req *http.Request) {
	defer req.Body.Close()

	query := req.URL.Query()
	callback := h.getQueryField(query, "callback")
	q := h.getQueryField(query, "q")

	response := h.dispatch([]byte(q), req.Host)
	data, err := json.Marshal(response)
	if err != nil {
		log.Print(err)
		return
	}

	if callback != "" {
		w.Header().Set("Content-Type", "application/javascript")
		fmt.Fprintf(w, "%s(", callback)
		w.Write(data)
		fmt.Fprint(w, ")")
	} else {
		w.Header().Set("Content-Type", "application/json")
		w.Write(data)
	}
}

func (h *httpHandler) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if strings.HasSuffix(req.URL.Path, "/ws/") {
		h.webSocketHandler.ServeHTTP(w, req)
	} else {
		h.serveApiRequest(w, req)
	}
}

func (s *Server) Handler() http.Handler {
	return newHttpHandler(s)
}

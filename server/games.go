package server

import (
	"fmt"
	"github.com/lanior/mysterious-bomberman/engine"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"math/rand"
	"time"
)

var (
	ErrAlreadyInGame      = ResponseStatus("already_in_game")
	ErrGameAlreadyStarted = ResponseStatus("game_already_started")
	ErrGameIsFull         = ResponseStatus("game_is_full")
	ErrGameNotFound       = ResponseStatus("game_not_found")
	ErrInvalidPlayerCount = ResponseStatus("invalid_player_count")
	ErrNotInGame          = ResponseStatus("not_in_game")
	ErrWrongGamePassword  = ResponseStatus("wrong_game_password")
)

type CreateGame struct {
	Name           string `format:".{2,32}"`
	Password       string `format:".{0,256}"`
	MapId          int
	MaxPlayerCount int
	Settings       GameSettings
}

type CreateGameResponse struct {
	GameId int
}

type JoinGame struct {
	GameId   int
	Password string `format:".{0,256}"`
}

type JoinGameResponse struct {
	MapId    int
	Settings GameSettings
}

type LeaveGame struct{}
type StartGame struct{}
type ListGames struct{}

type GameDetails struct {
	Id          int
	Name        string `format:".{2,32}"`
	TotalSlots  int
	FreeSlots   int
	HasPassword bool
	MapId       int
}

type ListGamesResponse struct {
	Games []*GameDetails
}

type SendMessage struct {
	Message string `format:".{1,256}"`
}

type WSNewMessage struct {
	Text      string `format:".{1,256}"`
	Username  string `format:"[a-zA-Z0-9_]{3,15}"`
	Timestamp int64
}

type WSPlayerJoinedGame struct {
	Username string `format:"[a-zA-Z0-9_]{3,15}"`
}

type WSPlayerLeftGame struct {
	Username string `format:"[a-zA-Z0-9_]{3,15}"`
}

type WSVoteStart struct {
	Username string `format:"[a-zA-Z0-9_]{3,15}"`
}

type WSGameStarted struct {
	Players            []string
	CurrentPlayerIndex int
}

type WSGameEnd struct {
	Winner string `format:"([a-zA-Z0-9_]{3,15}|)"`
}

type GamePlayerState struct {
	X int
	Y int

	LookDirection      string
	MovementDirection  string
	DirectionChangedAt int
	Speed              int
	RemainingBombs     int
	FlameLength        int
	Dead               bool
}

type BombState struct {
	X int
	Y int

	MovementDirection string `field:",omitempty"`
	Speed             int    `field:",omitempty"`
	FlameLength       int
	CreatedAt         int
	DestroyAt         int
}

type ExplosionState struct {
	X int
	Y int

	CreatedAt int
	DestroyAt int

	UpLength    int
	DownLength  int
	LeftLength  int
	RightLength int
}

type WSGameState struct {
	Tick       int
	Field      []string
	EngineTime int `field:",omitempty"`

	Players    []GamePlayerState
	Bombs      []BombState
	Explosions []ExplosionState
}

type WSGameAction struct {
	Direction string `format:"none|left|right|up|down"`
	Action    string `format:"none|place_bomb"`
}

func validateItemDescriptions(items map[string]ItemDescription) error {
	for _, item := range engine.ItemTypes {
		if item == engine.ItemRandom {
			continue
		}

		isDisease := false
		for _, disease := range engine.Diseases {
			if item == disease {
				isDisease = true
				break
			}
		}

		value, ok := items[item.Name]
		switch {
		case
			!ok,
			value.MaxCount < value.StartCount,
			value.Weight < 0,
			value.StartCount < 0,
			value.MaxCount < 0,
			item == engine.ItemSpeedUp && value.SpeedBonus <= 0,
			item == engine.ItemBomb && value.BombBonus <= 0,
			(item == engine.ItemFlameUp || item == engine.ItemGoldenFlame) && value.FlameBonus <= 0,
			(item == engine.ItemLightSpeed || item == engine.ItemTurtle) && value.Speed <= 0,
			isDisease && value.Duration <= 0:

			return fmt.Errorf("invalid item description: %s", item.Name)
		}
	}
	return nil
}

func ValidateGameSettings(settings *GameSettings) error {
	return validateItemDescriptions(settings.Items)
}

func (s *Server) CreateGame(request *CreateGame, user *User) (Response, error) {
	if user.GameId.Valid() {
		return nil, ErrAlreadyInGame
	}

	game := &Game{Name: request.Name}
	if err := game.Password.Set(request.Password); err != nil {
		return nil, err
	}

	if err := ValidateGameSettings(&request.Settings); err != nil {
		return nil, err
	}

	publicId, err := s.nextCounterValue("Games")
	if err != nil {
		return nil, err
	}

	var gameMap Map
	if err := s.maps.Find(bson.M{"public_id": request.MapId}).One(&gameMap); err != nil {
		if err == mgo.ErrNotFound {
			return nil, ErrMapNotFound
		}
		return nil, err
	}

	if request.MaxPlayerCount < 2 || request.MaxPlayerCount > gameMap.MaxPlayerCount {
		return nil, ErrInvalidPlayerCount
	}

	game.Id = bson.NewObjectId()
	game.PublicId = publicId
	game.TotalSlots = request.MaxPlayerCount
	game.FreeSlots = game.TotalSlots - 1
	game.Users = []*GameUser{&GameUser{UserId: user.Id, Username: user.Username, ReadyToStart: false}}
	game.MapPublicId = gameMap.PublicId
	game.Settings = request.Settings
	if err := s.games.Insert(game); err != nil {
		return nil, err
	}
	if err := s.users.UpdateId(user.Id, bson.M{"$set": bson.M{"game_id": game.Id}}); err != nil {
		return nil, err
	}
	s.sendMessageById(user.Id, &WSPlayerJoinedGame{Username: user.Username})

	s.gameIdsLock.Lock()
	s.gameIdByUserId[user.Id] = game.Id
	s.gameIdsLock.Unlock()

	return &CreateGameResponse{GameId: game.PublicId}, nil
}

func (s *Server) JoinGame(request *JoinGame, user *User) (Response, error) {
	if user.GameId.Valid() {
		return nil, ErrAlreadyInGame
	}

	var game Game
	if err := s.games.Find(bson.M{"public_id": request.GameId}).One(&game); err != nil {
		if err == mgo.ErrNotFound {
			return nil, ErrGameNotFound
		}
		return nil, err
	}

	switch {
	case !game.Password.EqualsTo(request.Password):
		return nil, ErrWrongGamePassword
	case game.FreeSlots == 0:
		return nil, ErrGameIsFull
	case game.HasStarted():
		return nil, ErrGameAlreadyStarted
	}

	var err error
	err = s.users.UpdateId(user.Id, bson.M{
		"$set": bson.M{
			"game_id":        game.Id,
			"ready_to_start": false,
		},
	})
	if err != nil {
		return nil, err
	}

	err = s.games.UpdateId(game.Id, bson.M{
		"$inc": bson.M{"free_slots": -1},
		"$push": bson.M{"users": &GameUser{
			UserId:       user.Id,
			Username:     user.Username,
			ReadyToStart: user.ReadyToStart,
		}},
	})
	if err != nil {
		return nil, err
	}

	s.gameBroadcast(&game, &WSPlayerJoinedGame{Username: user.Username})
	for _, gameUser := range game.Users {
		s.sendMessageById(user.Id, &WSPlayerJoinedGame{Username: gameUser.Username})
		if gameUser.ReadyToStart {
			s.sendMessageById(user.Id, &WSVoteStart{Username: gameUser.Username})
		}
	}
	s.sendMessageById(user.Id, &WSPlayerJoinedGame{Username: user.Username})

	s.gameIdsLock.Lock()
	s.gameIdByUserId[user.Id] = game.Id
	s.gameIdsLock.Unlock()

	return &JoinGameResponse{
		MapId:    game.MapPublicId,
		Settings: game.Settings,
	}, nil
}

func (s *Server) startGame(user *User, game *Game) error {
	var gameUsers []string
	for _, gameUser := range game.Users {
		gameUsers = append(gameUsers, gameUser.Username)
	}
	for i, gameUser := range game.Users {
		s.sendMessageById(gameUser.UserId, &WSGameStarted{Players: gameUsers, CurrentPlayerIndex: i})
	}

	gameEngine, err := s.createEngine(game)
	if err != nil {
		return err
	}

	if !game.DebugMode {
		go s.gameLoop(game, gameEngine)
	}
	return nil
}

func (s *Server) LeaveGame(request *LeaveGame, user *User, game *Game) (Response, error) {
	readyToStartDelta := 0
	gameStarted := game.HasStarted()
	if user.ReadyToStart && !gameStarted {
		readyToStartDelta--
	}

	var err error
	if game.FreeSlots+1 == game.TotalSlots {
		err = s.games.RemoveId(game.Id)
	} else {
		err = s.games.UpdateId(game.Id, bson.M{
			"$inc": bson.M{
				"ready_to_start": readyToStartDelta,
				"free_slots":     1,
			},
			"$pull": bson.M{"users": bson.M{"user_id": user.Id}},
		})
	}
	if err != nil {
		return nil, err
	}

	err = s.users.UpdateId(user.Id, bson.M{"$unset": bson.M{"game_id": 1}, "$set": bson.M{"ready_to_start": false}})
	if err != nil {
		return nil, err
	}

	s.enginesLock.Lock()
	delete(s.enginesByUserId, user.Id)
	s.enginesLock.Unlock()

	s.gameIdsLock.Lock()
	delete(s.gameIdByUserId, user.Id)
	s.gameIdsLock.Unlock()

	game.FreeSlots++
	s.gameBroadcast(game, &WSPlayerLeftGame{Username: user.Username})
	if !gameStarted && game.HasStarted() {
		if err := s.startGame(user, game); err != nil {
			return nil, err
		}
	}
	return nil, StatusOk
}

func (s *Server) StartGame(request *StartGame, user *User, game *Game) (Response, error) {
	if user.ReadyToStart {
		return nil, StatusOk
	}
	if err := s.users.UpdateId(user.Id, bson.M{"$set": bson.M{"ready_to_start": true}}); err != nil {
		return nil, err
	}

	if err := s.games.UpdateId(game.Id, bson.M{
		"$inc": bson.M{"ready_to_start": 1},
	}); err != nil {
		return nil, err
	}

	if err := s.games.Update(
		bson.M{"_id": game.Id, "users.user_id": user.Id},
		bson.M{"$set": bson.M{"users.$.ready_to_start": true}},
	); err != nil {
		return nil, err
	}

	s.gameBroadcast(game, &WSVoteStart{Username: user.Username})

	game.ReadyToStart++
	if game.HasStarted() {
		if err := s.startGame(user, game); err != nil {
			return nil, err
		}
	}
	return nil, StatusOk
}

func (s *Server) gameBroadcast(game *Game, message interface{}) {
	s.gameIdsLock.RLock()
	for _, user := range game.Users {
		if s.gameIdByUserId[user.UserId] == game.Id {
			s.sendMessageById(user.UserId, message)
		}
	}
	s.gameIdsLock.RUnlock()
}

func (s *Server) createEngine(game *Game) (*engine.Engine, error) {
	var gameMap Map
	if err := s.maps.Find(bson.M{"public_id": game.MapPublicId}).One(&gameMap); err != nil {
		return nil, err
	}

	mapDescription, err := engine.LoadMap(gameMap.Field)
	if err != nil {
		return nil, err
	}

	players := make([]engine.Player, len(game.Users))
	for i := range game.Users {
		players[i] = engine.Player(i)
	}

	var randomSeed uint32
	if game.DebugMode {
		randomSeed = game.RandomSeed
	} else {
		randomSeed = rand.Uint32()
	}

	config := &engine.GameConfiguration{
		RandomSeed:      randomSeed,
		MaxSlideLength:  game.Settings.MaxSlideLength,
		PlayerBaseSpeed: game.Settings.PlayerBaseSpeed,

		BombSpeed:              game.Settings.BombSpeed,
		BombDelay:              game.Settings.BombDelay,
		BombChainReactionDelay: game.Settings.BombChainReactionDelay,

		BlastWaveDuration:   game.Settings.BlastWaveDuration,
		CellDecayDuration:   game.Settings.CellDecayDuration,
		ItemDropProbability: game.Settings.ItemDropProbability,

		Items: make(engine.ItemsConfigurationMap),
	}

	for _, item := range engine.ItemTypes {
		itemConfig, ok := game.Settings.Items[item.Name]
		if !ok {
			return nil, fmt.Errorf("invalid items config: missing item %s", item.Name)
		}

		config.Items[item] = &engine.ItemConfiguration{
			Weight:     itemConfig.Weight,
			StartCount: itemConfig.StartCount,
			MaxCount:   itemConfig.MaxCount,

			Speed:    itemConfig.Speed,
			Duration: itemConfig.Duration,

			SpeedBonus: itemConfig.SpeedBonus,
			FlameBonus: itemConfig.FlameBonus,
			BombBonus:  itemConfig.BombBonus,
		}
	}

	config.Items[engine.ItemInvert].InvertMovement = true
	config.Items[engine.ItemAutoBomb].AutoBomb = true
	config.Items[engine.ItemNoBomb].NoBomb = true

	for _, item := range engine.Diseases {
		config.Items[item].Group = 1
		config.Items[item].ClearGroup = 1
	}

	gameEngine, err := engine.NewEngine(config, mapDescription, players)
	if err != nil {
		return nil, err
	}

	s.enginesLock.Lock()
	for i, user := range game.Users {
		s.enginesByUserId[user.UserId] = &engineDescriptor{
			Engine:    gameEngine,
			Player:    engine.Player(i),
			DebugMode: game.DebugMode,
		}
	}
	s.enginesLock.Unlock()
	return gameEngine, nil
}

func (s *Server) gameLoop(game *Game, gameEngine *engine.Engine) {
	startTime := time.Now()
	ticker := time.NewTicker(engine.TickPeriod)
	for _ = range ticker.C {
		for time.Now().Sub(startTime) > time.Duration(gameEngine.State.Tick+1)*engine.TickPeriod {
			stateMessage := s.prepareGameStateMessage(gameEngine.State)
			gameEngine.Tick()

			s.gameBroadcast(game, stateMessage)

			if gameEngine.GameEnd() {
				winner := ""
				winnerPos := gameEngine.Winner()
				if winnerPos != -1 {
					winner = game.Users[winnerPos].Username
				}
				s.gameBroadcast(game, &WSGameEnd{Winner: winner})
				ticker.Stop()
			}
		}
	}
}

var directionNames = map[engine.Vector2]string{
	engine.Up:    "up",
	engine.Down:  "down",
	engine.Left:  "left",
	engine.Right: "right",
}

func TranslateDirection(direction engine.Vector2) string {
	if repr, ok := directionNames[direction]; ok {
		return repr
	}
	return "none"
}

func (s *Server) prepareGameStateMessage(state *engine.GameState) *WSGameState {
	mapWidth := state.Map.Description.Width
	mapHeight := state.Map.Description.Height

	i := 0
	cells := state.Map.Cells
	row := make([]byte, mapWidth)
	field := make([]string, mapHeight)
	for y := 0; y < mapHeight; y++ {
		for x := 0; x < mapWidth; x++ {
			cell := &cells[i]
			if cell.ItemType != nil {
				row[x] = byte(cell.ItemType.Code)
			} else if cell.DestroyAt != -1 {
				row[x] = '$'
			} else {
				row[x] = byte(cell.Block.Code)
			}
			i++
		}
		field[y] = string(row)
	}

	players := make([]GamePlayerState, len(state.Players))
	for i, player := range state.Players {
		players[i] = GamePlayerState{
			X:                  player.Pos.X,
			Y:                  player.Pos.Y,
			Speed:              player.Speed,
			Dead:               player.Dead,
			RemainingBombs:     player.MaxBombs - player.PlacedBombs,
			FlameLength:        player.BlastRadius,
			LookDirection:      TranslateDirection(player.LookDirection),
			MovementDirection:  TranslateDirection(player.MovementDirection),
			DirectionChangedAt: player.DirectionChangedAt,
		}
	}

	bombs := make([]BombState, len(state.Bombs))
	for i, bomb := range state.Bombs {
		bombs[i] = BombState{
			X:                 bomb.Pos.X,
			Y:                 bomb.Pos.Y,
			Speed:             bomb.Speed,
			FlameLength:       bomb.BlastRadius,
			CreatedAt:         bomb.CreatedAt,
			DestroyAt:         bomb.ExplodesAt,
			MovementDirection: TranslateDirection(bomb.MovementDirection),
		}
	}

	explosions := make([]ExplosionState, len(state.Explosions))
	for i, explosion := range state.Explosions {
		explosions[i] = ExplosionState{
			X: explosion.Pos.X,
			Y: explosion.Pos.Y,

			CreatedAt: explosion.CreatedAt,
			DestroyAt: explosion.DestroyAt,

			UpLength:    explosion.Up,
			DownLength:  explosion.Down,
			LeftLength:  explosion.Left,
			RightLength: explosion.Right,
		}
	}

	return &WSGameState{
		Field:      field,
		Tick:       state.Tick,
		Players:    players,
		Bombs:      bombs,
		Explosions: explosions,
		EngineTime: int(state.CalculationTime / 1000),
	}
}

func (s *Server) ListGames(request *ListGames, user *User) (Response, error) {
	var game Game

	games := []*GameDetails{}
	iter := s.games.Find(bson.M{"free_slots": bson.M{"$ne": 0}}).Iter()
	for iter.Next(&game) {
		if !game.HasStarted() {
			games = append(games, &GameDetails{
				Id:          game.PublicId,
				Name:        game.Name,
				TotalSlots:  game.TotalSlots,
				FreeSlots:   game.FreeSlots,
				HasPassword: !game.Password.EqualsTo(""),
				MapId:       game.MapPublicId,
			})
		}
	}
	if iter.Err() != nil {
		return nil, iter.Err()
	}
	return &ListGamesResponse{Games: games}, nil
}

func (s *Server) SendMessage(request *SendMessage, user *User, game *Game) (Response, error) {
	message := &Message{
		UserId:    user.Id,
		Username:  user.Username,
		Message:   request.Message,
		Timestamp: time.Now().Unix(),
	}

	update := bson.M{"$push": bson.M{"messages": message}}
	if err := s.games.UpdateId(game.Id, update); err != nil {
		return nil, err
	}

	s.gameBroadcast(game, &WSNewMessage{
		Username:  user.Username,
		Text:      message.Message,
		Timestamp: message.Timestamp,
	})
	return nil, StatusOk
}

func (s *Server) WSGameAction(request *WSGameAction, conn PersistentConn, userId bson.ObjectId) error {
	s.applyGameAction(userId, request.Direction, request.Action)
	return nil
}

func (s *Server) applyGameAction(userId bson.ObjectId, movement, action string) {
	descriptor, ok := s.enginesByUserId[userId]
	if !ok {
		return
	}

	direction := engine.None
	switch movement {
	case "left":
		direction = engine.Left
	case "right":
		direction = engine.Right
	case "up":
		direction = engine.Up
	case "down":
		direction = engine.Down
	}
	descriptor.Engine.SetMovementDirection(descriptor.Player, direction)
	if action == "place_bomb" {
		descriptor.Engine.PlaceBomb(descriptor.Player)
	} else {
		descriptor.Engine.ResetAction(descriptor.Player)
	}
}

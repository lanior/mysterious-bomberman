package server

import (
	"github.com/lanior/scheme"
	"labix.org/v2/mgo/bson"
	"reflect"
)

type sidStruct struct {
	Sid string `format:"[a-zA-Z0-9]{32}"`
}

type eventStruct struct {
	Event string
}

var (
	sidScheme   *scheme.Scheme
	eventScheme *scheme.Scheme
)

func init() {
	var err error

	sidScheme, err = scheme.DescribeUsing(&sidStruct{}, scheme.Underscores)
	if err != nil {
		panic(err)
	}

	eventScheme, err = scheme.DescribeUsing(&eventStruct{}, scheme.Underscores)
	if err != nil {
		panic(err)
	}
}

func (s *Server) dispatch(message map[string]interface{}, host string) (map[string]interface{}, error) {
	actionField, ok := message["action"]
	if !ok {
		return nil, ErrMalformedRequest
	}

	action, ok := actionField.(string)
	if !ok {
		return nil, ErrMalformedRequest
	}

	info, ok := s.actions[action]
	if !ok {
		return nil, ErrUnsupportedAction
	}

	paramStruct := reflect.New(info.Type.In(0).Elem())
	if err := info.Scheme.Unmarshal(message, paramStruct.Interface()); err != nil {
		return nil, err
	}

	args := make([]reflect.Value, info.Type.NumIn())
	args[0] = paramStruct

	if info.UserPos != -1 {
		var actionSid sidStruct
		if err := sidScheme.Unmarshal(message, &actionSid); err != nil {
			return nil, err
		}

		var user *User
		if err := s.users.Find(bson.M{"sid": actionSid.Sid}).One(&user); err != nil {
			return nil, ErrInvalidSid
		}
		args[info.UserPos] = reflect.ValueOf(user)

		if info.GamePos != -1 {
			if !user.GameId.Valid() {
				return nil, ErrNotInGame
			}

			var game *Game
			if err := s.games.FindId(user.GameId).One(&game); err != nil {
				return nil, ErrInternalServerError
			}
			args[info.GamePos] = reflect.ValueOf(game)
		}
	}

	if info.HostPos != -1 {
		args[info.HostPos] = reflect.ValueOf(hostName(host))
	}

	result := info.Value.Call(args)
	if !result[1].IsNil() {
		err := result[1].Interface().(error)
		if err != nil {
			return nil, err
		}
	}

	if result[0].IsNil() {
		return nil, StatusOk
	}

	response := result[0].Interface().(Response)
	structScheme, err := scheme.DescribeUsing(response, scheme.Underscores)
	if err != nil {
		return nil, err
	}
	return structScheme.Marshal(response)
}

func (s *Server) Dispatch(message map[string]interface{}, host string) map[string]interface{} {
	result, err := s.dispatch(message, host)
	if err != nil {
		response := map[string]interface{}{
			"status": "malformed_request",
		}

		if status, ok := err.(ResponseStatus); ok {
			response["status"] = status.String()
		} else if s.debug {
			response["error"] = err.Error()
		}
		return response
	}
	if _, ok := result["status"]; !ok {
		result["status"] = "ok"
	}
	return result
}

func (s *Server) dispatchUserMessage(conn PersistentConn, message map[string]interface{}) interface{} {
	var eventHeader eventStruct
	if err := eventScheme.Unmarshal(message, &eventHeader); err != nil {
		return &WSProtocolViolation{}
	}

	event, ok := s.events[eventHeader.Event]
	if !ok {
		return &WSProtocolViolation{}
	}

	paramStruct := reflect.New(event.Type.In(0).Elem())
	if err := event.Scheme.Unmarshal(message, paramStruct.Interface()); err != nil {
		return &WSProtocolViolation{}
	}

	s.connLock.RLock()
	userId := s.conns[conn]
	s.connLock.RUnlock()

	if (eventHeader.Event == "auth") != !userId.Valid() {
		return &WSProtocolViolation{}
	}

	args := make([]reflect.Value, event.Type.NumIn())
	args[0] = paramStruct
	args[1] = reflect.ValueOf(conn)
	args[2] = reflect.ValueOf(userId)

	result := event.Value.Call(args)
	if !result[0].IsNil() {
		return &WSInternalServerError{}
	}
	return nil
}

func (s *Server) DispatchUserMessage(conn PersistentConn, message map[string]interface{}) {
	if errMessage := s.dispatchUserMessage(conn, message); errMessage != nil {
		s.sendClosingMessageToConn(conn, errMessage)
	}
}

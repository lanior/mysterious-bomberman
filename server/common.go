package server

import (
	"github.com/lanior/mysterious-bomberman/engine"
)

var (
	StatusOk = ResponseStatus("ok")

	ErrInternalServerError = ResponseStatus("malformed_request")
	ErrMalformedRequest    = ResponseStatus("malformed_request")
	ErrUnsupportedAction   = ResponseStatus("unsupported_action")
)

type Extensions struct{}

type ExtensionsResponse struct {
	Extensions map[string]interface{}
}

func (s ResponseStatus) String() string {
	return string(s)
}

func (s ResponseStatus) Error() string {
	return s.String()
}

func (s *Server) getDefaultSettings() *GameSettings {
	settings := s.getDefaultSettingsDict()

	items := make(map[string]ItemDescription)
	for name, rawItem := range settings["items"].(map[string]interface{}) {
		item := rawItem.(map[string]interface{})

		get := func(key string) int {
			if v, ok := item[key]; ok {
				return v.(int)
			}
			return 0
		}

		items[name] = ItemDescription{
			Weight:     get("weight"),
			StartCount: get("start_count"),
			MaxCount:   get("max_count"),
			Speed:      get("speed"),
			Duration:   get("duration"),
			SpeedBonus: get("speed_bonus"),
			FlameBonus: get("flame_bonus"),
			BombBonus:  get("bomb_bonus"),
		}
	}
	return &GameSettings{
		MaxSlideLength:         settings["max_slide_length"].(int),
		PlayerBaseSpeed:        settings["player_base_speed"].(int),
		BombSpeed:              settings["bomb_speed"].(int),
		BombDelay:              settings["bomb_delay"].(int),
		BombChainReactionDelay: settings["bomb_chain_reaction_delay"].(int),
		BlastWaveDuration:      settings["blast_wave_duration"].(int),
		CellDecayDuration:      settings["cell_decay_duration"].(int),
		ItemDropProbability:    settings["item_drop_probability"].(int),
		Items:                  items,
	}
}

func (s *Server) getDefaultSettingsDict() map[string]interface{} {
	dc := engine.DefaultGameConfiguration
	items := make(map[string]interface{})
	for item, config := range dc.Items {
		c := make(map[string]interface{})
		put := func(name string, value int) {
			if value > 0 {
				c[name] = value
			}
		}

		c["weight"] = config.Weight
		c["start_count"] = config.StartCount
		c["max_count"] = config.MaxCount

		put("speed", config.Speed)
		put("duration", config.Duration)

		put("speed_bonus", config.SpeedBonus)
		put("flame_bonus", config.FlameBonus)
		put("bomb_bonus", config.BombBonus)

		items[item.Name] = c
	}

	return map[string]interface{}{
		"max_slide_length":          dc.MaxSlideLength,
		"player_base_speed":         dc.PlayerBaseSpeed,
		"bomb_speed":                dc.BombSpeed,
		"bomb_delay":                dc.BombDelay,
		"bomb_chain_reaction_delay": dc.BombChainReactionDelay,
		"blast_wave_duration":       dc.BlastWaveDuration,
		"cell_decay_duration":       dc.CellDecayDuration,
		"item_drop_probability":     dc.ItemDropProbability,
		"items":                     items,
	}
}

func (s *Server) Extensions(request *Extensions, host hostName) (Response, error) {
	webSocketUrl := s.webSocketUrl
	if webSocketUrl == "auto" {
		webSocketUrl = "ws://" + string(host) + "/api/ws/"
	}

	extensions := map[string]interface{}{
		"core.v0": map[string]interface{}{
			"websocket_url":         webSocketUrl,
			"min_map_size":          engine.MinMapSize,
			"max_map_size":          engine.MaxMapSize,
			"default_game_settings": s.getDefaultSettingsDict(),
		},
	}

	if s.debug {
		extensions["debug.v0"] = nil
	}
	return &ExtensionsResponse{Extensions: extensions}, nil
}

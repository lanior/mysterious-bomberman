package server

import (
	"bytes"
	"fmt"
	"github.com/lanior/mysterious-bomberman/engine"
	"github.com/lanior/scheme"
	"io/ioutil"
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
	"log"
	"path"
	"path/filepath"
	"reflect"
	"strings"
	"sync"
	"unicode"
)

const ErrMongoDupKey = 11000

type Server struct {
	debug        bool
	webSocketUrl string

	logger *log.Logger

	db       *mgo.Database
	counters *mgo.Collection
	games    *mgo.Collection
	users    *mgo.Collection
	maps     *mgo.Collection

	actions map[string]*actionInfo
	events  map[string]*actionInfo

	connLock      sync.RWMutex
	conns         map[PersistentConn]bson.ObjectId
	connsByUserId map[bson.ObjectId]PersistentConn

	enginesLock     sync.RWMutex
	enginesByUserId map[bson.ObjectId]*engineDescriptor

	gameIdsLock    sync.RWMutex
	gameIdByUserId map[bson.ObjectId]bson.ObjectId
}

type engineDescriptor struct {
	Engine *engine.Engine
	Player engine.Player

	DebugMode     bool
	DebugModeLock sync.Mutex
}

type actionInfo struct {
	Type   reflect.Type
	Value  reflect.Value
	Scheme *scheme.Scheme

	UserPos int
	GamePos int
	HostPos int
}

type Config struct {
	DebugMode    bool
	WebSocketUrl string
	DBConfig     *mgo.DialInfo
	Logger       *log.Logger
}

type Response interface{}
type ResponseStatus string

type PersistentConn interface {
	Send(message map[string]interface{}) error
	Close() error
}

type WSProtocolViolation struct{}
type WSInternalServerError struct{}

type hostName string

var serviceFunctions = []string{
	"CloseUserConn",
	"Dispatch",
	"DispatchUserMessage",
	"Handler",
	"LoadMaps",
	"RegisterUserConn",
}

func New(config *Config) (*Server, error) {
	session, err := mgo.DialWithInfo(config.DBConfig)
	if err != nil {
		return nil, fmt.Errorf("MongoDD: %s", err)
	}

	db := session.DB(config.DBConfig.Database)
	server := &Server{
		debug:        config.DebugMode,
		webSocketUrl: config.WebSocketUrl,
		db:           db,
		logger:       config.Logger,

		counters: db.C("Counters"),
		games:    db.C("Games"),
		users:    db.C("Users"),
		maps:     db.C("Maps"),

		actions:         make(map[string]*actionInfo),
		events:          make(map[string]*actionInfo),
		conns:           make(map[PersistentConn]bson.ObjectId),
		connsByUserId:   make(map[bson.ObjectId]PersistentConn),
		enginesByUserId: make(map[bson.ObjectId]*engineDescriptor),
		gameIdByUserId:  make(map[bson.ObjectId]bson.ObjectId),
	}

	if err := server.initActions(); err != nil {
		return nil, fmt.Errorf("actions init error: %s", err)
	}

	if err := server.initDbIndexes(); err != nil {
		return nil, fmt.Errorf("MongoDD: %s", err)
	}
	return server, nil
}

func (s *Server) initActions() error {
	findArgPos := func(t reflect.Type, argTypeV interface{}) int {
		argType := reflect.TypeOf(argTypeV)
		for i := 0; i < t.NumIn(); i++ {
			if t.In(i) == argType {
				return i
			}
		}
		return -1
	}

	v := reflect.ValueOf(s)
	t := v.Type()
	for i := 0; i < t.NumMethod(); i++ {
		mv := v.Method(i)
		mt := t.Method(i)

		if unicode.IsLower(rune(mt.Name[0])) {
			continue
		}

		found := false
		for _, fn := range serviceFunctions {
			if fn == mt.Name {
				found = true
				break
			}
		}

		if found {
			continue
		}

		argScheme, err := scheme.DescribeUsing(mt.Type.In(1), scheme.Underscores)
		if err != nil {
			return err
		}

		info := &actionInfo{
			Type:    mv.Type(),
			Value:   mv,
			Scheme:  argScheme,
			UserPos: findArgPos(mv.Type(), &User{}),
			GamePos: findArgPos(mv.Type(), &Game{}),
			HostPos: findArgPos(mv.Type(), hostName("")),
		}

		action := scheme.ToUnderscores(mt.Name)
		if strings.HasPrefix(mt.Name, "WS") {
			s.events[action[2:]] = info
		} else {
			s.actions[action] = info
		}
	}
	return nil
}

func (s *Server) initDbIndexes() error {
	loginIndex := mgo.Index{
		Key:    []string{"username"},
		Unique: true,
	}
	if err := s.users.EnsureIndex(loginIndex); err != nil {
		return err
	}

	sidIndex := mgo.Index{
		Key:    []string{"sid"},
		Unique: true,
		Sparse: true,
	}
	if err := s.users.EnsureIndex(sidIndex); err != nil {
		return err
	}

	if err := s.games.EnsureIndexKey("public_id"); err != nil {
		return err
	}

	if err := s.maps.EnsureIndexKey("public_id"); err != nil {
		return err
	}
	return nil
}

func (s *Server) nextCounterValue(name string) (result int, err error) {
	cmd := bson.D{
		{"findAndModify", s.counters.Name},
		{"query", bson.D{{"name", name}}},
		{"update", bson.M{"$inc": bson.M{"counter": 1}}},
		{"upsert", true},
	}

	res := struct {
		LastError mgo.LastError `bson:"lastErrorObject"`
		Value     struct {
			Counter int
		}
	}{}

	if err = s.db.Run(cmd, &res); err != nil {
		return
	}
	if res.LastError.UpdatedExisting {
		return res.Value.Counter + 1, nil
	}
	return 1, nil
}

func (s *Server) LoadMaps(dir string) {
	loadStandardMap := func(name string, field []string) error {
		_, err := s.uploadMap(&UploadMap{
			Name:     name,
			Field:    field,
			Settings: *s.getDefaultSettings(),
		}, true)
		return err
	}

	loadClanBomberMap := func(name string, lines []string) error {
		if len(lines) < 2 {
			return fmt.Errorf("invalid map format")
		}

		var result []string
		for _, inputLine := range lines[2:] {
			line := make([]byte, len(inputLine)*2)
			for i, cell := range inputLine {
				var a, b byte
				switch cell {
				case '*':
					a = '#'
				case '+', 'R':
					a = '%'
				case '^', 'v', '<', '>':
					a = byte(cell)
				case ' ':
					a = '.'
				case '-':
					a = '*'
				default:
					if cell >= '0' && cell < '9' {
						a = byte(cell)
						b = '.'
					} else {
						a = '.'
					}
				}

				if b == 0 {
					b = a
				}

				line[i*2] = a
				line[i*2+1] = b
			}
			result = append(result, string(line))
		}
		return loadStandardMap(name, result)
	}

	loadMap := func(filePath string) (err error) {
		loaders := []func(string, []string) error{
			loadStandardMap,
			loadClanBomberMap,
		}

		content, err := ioutil.ReadFile(filePath)
		if err != nil {
			return err
		}

		_, name := path.Split(filePath)
		name = strings.SplitN(name, ".", 2)[0]
		for _, loadFn := range loaders {
			var lines []string
			for _, byteLine := range bytes.Split(content, []byte{'\n'}) {
				line := strings.Trim(string(byteLine), "\n")
				if line != "" {
					lines = append(lines, line)
				}
			}

			if err = loadFn(name, lines); err == nil {
				return nil
			}
		}
		return
	}

	if _, err := s.maps.RemoveAll(bson.M{"preloaded": true}); err != nil {
		s.logger.Printf("LoadMaps: cannot remove old maps: %s", err)
	}

	matches, err := filepath.Glob(dir + "*.map")
	if err != nil {
		s.logger.Printf("LoadMaps: Glob: %s", err)
		return
	}

	for _, match := range matches {
		result := "ok"
		if err := loadMap(match); err != nil {
			result = fmt.Sprintf("failed (%s)", err)
		}
		s.logger.Printf(`loading map "%s": %s`, match[len(dir):], result)
	}
}

func (s *Server) RegisterUserConn(conn PersistentConn) {
	s.connLock.Lock()
	s.conns[conn] = bson.ObjectId("")
	s.connLock.Unlock()
}

func (s *Server) CloseUserConn(conn PersistentConn) {
	s.connLock.Lock()
	if userId, ok := s.conns[conn]; ok {
		delete(s.conns, conn)
		delete(s.connsByUserId, userId)
	}
	s.connLock.Unlock()
	conn.Close()
}

func (s *Server) sendMessageById(userId bson.ObjectId, message interface{}) {
	s.connLock.RLock()
	conn, ok := s.connsByUserId[userId]
	s.connLock.RUnlock()
	if !ok {
		return
	}
	s.sendMessageToConn(conn, message)
}

func (s *Server) sendMessageToConn(conn PersistentConn, message interface{}) {
	go func() {
		value, err := marshalMessageObject(message)
		if err != nil {
			s.CloseUserConn(conn)
			return
		}

		if err := conn.Send(value); err != nil {
			s.CloseUserConn(conn)
		}
	}()
}

func (s *Server) sendClosingMessageToConn(conn PersistentConn, message interface{}) {
	go func() {
		value, err := marshalMessageObject(message)
		if err != nil {
			s.CloseUserConn(conn)
			return
		}

		_ = conn.Send(value)
		s.CloseUserConn(conn)
	}()
}

func marshalMessageObject(obj interface{}) (map[string]interface{}, error) {
	if status, ok := obj.(ResponseStatus); ok {
		return map[string]interface{}{
			"event": status.String(),
		}, nil
	}

	messageScheme, err := scheme.DescribeUsing(obj, scheme.Underscores)
	if err != nil {
		return nil, err
	}

	result, err := messageScheme.Marshal(obj)
	if err != nil {
		return nil, err
	}

	if _, ok := result["event"]; !ok {
		name := reflect.TypeOf(obj).Elem().Name()
		if strings.HasPrefix(name, "WS") {
			result["event"] = scheme.ToUnderscores(name[2:])
		}
	}
	return result, nil
}

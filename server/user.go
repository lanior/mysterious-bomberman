package server

import (
	"labix.org/v2/mgo"
	"labix.org/v2/mgo/bson"
)

var (
	ErrInvalidCredentials = ResponseStatus("invalid_credentials")
	ErrInvalidSid         = ResponseStatus("invalid_sid")
	ErrUserAlreadyExists  = ResponseStatus("user_already_exists")
)

type Register struct {
	Username string `format:"[a-zA-Z0-9_]{3,15}"`
	Password string `format:".{1,256}"`
}

type Login struct {
	Username string `format:"[a-zA-Z0-9_]{3,15}"`
	Password string `format:".{1,256}"`
}

type LoginResponse struct {
	Sid string `format:"[a-zA-Z0-9]{32}"`
}

type PlayerState struct{}

type PlayerStateResponse struct {
	State    string `format:"not_in_game|in_game|in_started_game"`
	GameId   int    `bson:"game_id,omitempty"`
	GameName string `bson:"game_name,omitempty"`
}

type Logout struct{}

type WSAuth struct {
	Sid string `format:"[a-zA-Z0-9]{32}"`
}

type WSAuthSuccess struct{}
type WSAuthError struct{}
type WSAnotherConnectionOpened struct{}

func (s *Server) Register(request *Register) (Response, error) {
	user := &User{Username: request.Username}
	if err := user.Password.Set(request.Password); err != nil {
		return nil, err
	}

	if err := s.users.Insert(user); err != nil {
		if err.(*mgo.LastError).Code == ErrMongoDupKey {
			return nil, ErrUserAlreadyExists
		}
		return nil, err
	}
	return nil, StatusOk
}

func (s *Server) Login(request *Login) (Response, error) {
	var user User
	if err := s.users.Find(bson.M{"username": request.Username}).One(&user); err != nil {
		if err == mgo.ErrNotFound {
			return nil, ErrInvalidCredentials
		}
		return nil, err
	}

	if !user.Password.EqualsTo(request.Password) {
		return nil, ErrInvalidCredentials
	}

	sid, err := generateSid()
	if err != nil {
		return nil, err
	}

	if err := s.users.UpdateId(user.Id, bson.M{"$set": bson.M{"sid": sid}}); err != nil {
		return nil, err
	}
	return &LoginResponse{Sid: sid}, nil
}

func (s *Server) PlayerState(request *PlayerState, user *User) (Response, error) {
	if user.GameId.Valid() {
		var game Game
		if err := s.games.FindId(user.GameId).One(&game); err != nil {
			return nil, err
		}

		state := "in_game"
		if game.HasStarted() {
			state = "in_started_game"
		}

		return &PlayerStateResponse{
			GameId:   game.PublicId,
			GameName: game.Name,
			State:    state,
		}, nil
	}
	return &PlayerStateResponse{State: "not_in_game"}, nil
}

func (s *Server) Logout(request *Logout, user *User) (Response, error) {
	if err := s.users.UpdateId(user.Id, bson.M{"$unset": bson.M{"sid": 1}}); err != nil {
		return nil, err
	}
	if user.GameId.Valid() {
		var game Game
		if err := s.games.FindId(user.GameId).One(&game); err != nil {
			return nil, err
		}
		s.LeaveGame(&LeaveGame{}, user, &game)
	}
	return nil, StatusOk
}

func (s *Server) WSAuth(request *WSAuth, conn PersistentConn, userId bson.ObjectId) error {
	var user User
	if err := s.users.Find(bson.M{"sid": request.Sid}).One(&user); err != nil {
		if err == mgo.ErrNotFound {
			s.sendClosingMessageToConn(conn, &WSAuthError{})
			return nil
		}
		return err
	}
	s.connLock.Lock()
	if oldConn, ok := s.connsByUserId[user.Id]; ok {
		s.sendClosingMessageToConn(oldConn, &WSAnotherConnectionOpened{})
	}
	s.conns[conn] = user.Id
	s.connsByUserId[user.Id] = conn
	s.connLock.Unlock()
	s.sendMessageToConn(conn, &WSAuthSuccess{})

	if user.GameId.Valid() {
		var game Game
		if err := s.games.FindId(user.GameId).One(&game); err != nil {
			return err
		}
		for _, gameUser := range game.Users {
			s.sendMessageById(user.Id, &WSPlayerJoinedGame{Username: gameUser.Username})
			if gameUser.ReadyToStart {
				s.sendMessageById(user.Id, &WSVoteStart{Username: gameUser.Username})
			}
		}
		for _, message := range game.Messages {
			s.sendMessageById(user.Id, &WSNewMessage{
				Username:  message.Username,
				Text:      message.Message,
				Timestamp: message.Timestamp,
			})
		}
		if game.HasStarted() {
			var (
				gameUsers []string
				index     int
			)
			for i, gameUser := range game.Users {
				gameUsers = append(gameUsers, gameUser.Username)
				if gameUser.UserId == user.Id {
					index = i
				}
			}
			s.sendMessageById(user.Id, &WSGameStarted{Players: gameUsers, CurrentPlayerIndex: index})
		}
	}
	return nil
}

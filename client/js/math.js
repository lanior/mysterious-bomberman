function Vector2(x, y) {
    this.x = x;
    this.y = y;
}

Vector2.prototype.add = function(v) {
    return new Vector2(this.x + v.x, this.y + v.y);
};

Vector2.prototype.sub = function(v) {
    return new Vector2(this.x - v.x, this.y - v.y);
};

Vector2.prototype.mul = function(c) {
    return new Vector2(this.x * c, this.y * c);
};

Vector2.prototype.sign = function(v) {
    return new Vector2(sign(this.x), sign(this.y));
};

Vector2.prototype.copy = function() {
    return new Vector2(this.x, this.y);
};

Vector2.directions = {
    'left':   new Vector2(-1,  0),
    'right':  new Vector2( 1,  0),
    'up':     new Vector2( 0, -1),
    'down':   new Vector2( 0,  1)
};

Vector2.getDirection = function(name) {
    var v = Vector2.directions[name];
    return v ? v.copy() : new Vector2(0, 0);
};

function sign(v) {
    return v > 0 ? 1 : (v < 0 ? -1 : 0);
}

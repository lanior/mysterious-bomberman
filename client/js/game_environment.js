function GameEnvironment(transport, renderer, gameRenderer) {
    this.renderer = renderer;
    this.transport = transport;
    this.controller = new InputController(transport, DEFAULT_CONTROLS);
    this.controller.attach($(document));

    this.gameRenderer = gameRenderer;
    this.startTime = null;
    this.lastState = null;
    this.playerLabels = [];

    this.gameRenderer.clear();
    this.renderer.show();
    this.render();
}

GameEnvironment.prototype.render = function() {
    if (this.renderer == null) {
        return;
    }

    if (this.lastState != null) {
        var delta = new Date().getTime() - this.startTime;
        var playerPositions = this.gameRenderer.renderGameState(this.lastState, delta);
        _.each(this.playerLabels, function(label, i) {
            var x = playerPositions[i].x * scale - (label.width() + 3*2 - SPRITE_SIZE) / 2,
                y = playerPositions[i].y * scale - label.height() - 3;
            label.css({
                'left': x + 'px',
                'top': y + 'px'
            })
        })
        $('#game_field').scrollTop(currentPos.y * scale - gameFieldSize.y / 2);
        $('#game_field').scrollLeft(currentPos.x * scale  - gameFieldSize.x / 2);
    }
    window.requestAnimationFrame(_.bind(this.render, this));
};

GameEnvironment.prototype.initPlayerLabels = function(players, currentPlayerIndex) {
    var env = this;
    _.each(players, function(player, i) {
        var label = $('<div class="player_label">')
            .text(player)
            .appendTo(env.renderer.container);
        if (i == currentPlayerIndex) {
            label.addClass('current_player');
        }
        env.playerLabels.push(label);
    });
};

GameEnvironment.prototype.setState = function(state) {
    this.startTime = new Date().getTime() - state.tick * tickPeriod;

    var gameState = new GameState(state);
    if (this.lastState == null) {
        var label = this.playerLabels[state.current_player_index];
        if (label) {
            label.addClass('current_player');
        }
        this.renderer.resize(gameState.map.getVisualWidth(), gameState.map.getVisualHeight());
    }
    this.lastState = gameState;
};

GameEnvironment.prototype.destroy = function() {
    this.controller.detach($(document));
    this.renderer.hide();
    this.renderer = null;
    _.invoke(this.playerLabels, 'remove');
};

// var bonuses = {
//     'bomb': ['bomb_bonus'],
//     'flame': ['flame_bonus'],
//     'goldenflame': ['flame_bonus'],
//     'speed': ['speed_bonus'],
//     'kicker': [],
//     'throw': [],
//     'invert': ['duration'],
//     'lightspeed': ['duration', 'speed'],
//     'autobomb': ['duration'],
//     'nobomb': ['duration'],
//     'turtle': ['duration', 'speed']
// };

function createTable(id) {
    return $('<table>')
        .addClass('table')
        .addClass('table-striped')
        .attr('id', id);
}

function makeSettingsSet(defaultSettings) {
    var table = createTable('settings');
    _.each(defaultSettings, function(value, key) {
        if (key == 'items') {
            return;
        }
        var label = $('<label>').attr('for', key).text(key),
            input = $('<input>').attr('id', key).val(value),
            row = $('<tr>')
                .append($('<td>').append(label))
                .append($('<td>').append(input))
                .appendTo(table);
    });
    return table;
}

function makeItemsSet(defaultSettings) {
    var table = createTable('bonuses_table');
    _.each(defaultSettings.items, function(properties, key) {
        function addData(row, namePart, defaultVal) {
            var cell = $('<td>'),
                name = key + '_' + namePart,
                label = $('<label>')
                    .attr('for', name)
                    .text(namePart + ':')
                    .addClass('inline_label'),
                input = $('<input>')
                    .addClass('span1')
                    .addClass('labeled_input')
                    .attr('id', name)
                    .val(defaultVal),
                div = $('<div>')
                    .append(label)
                    .append(input)
                    .appendTo(cell);

            row.append(cell);
        }

        var row = $('<tr>')
            .append($('<td>').text(key))
            .appendTo(table);

        _.each(properties, function(value, property) {
            addData(row, property, value);
        });
    });
    return table;
}

function createSettingsField(settingsParentId, itemsParentId, values) {
    settingsParent = $('#' + settingsParentId + ' .settings').empty().append(makeSettingsSet(values));
    itemsParent = $('#' + itemsParentId).empty().append(makeItemsSet(values));
}

function collectSettings() {
    var settings = {};
    _.each(config.settings, function(v, key) {
        if (key == 'items') {
            var items = {};
            _.each(v, function(p, item) {
                var properties = {};
                _.each(p, function(value, property) {
                    properties[property] = parseInt($('#' + item + '_' + property).val());
                });
                items[item] = properties;
            });
            settings[key] = items;
        } else {
            settings[key] = parseInt($('#' + key).val());
        }
    });
    return settings;
}

function selectTab(tabNames, selected) {
    if (selected.hasClass('disabled')) {
        return;
    }
    _.each(tabNames, function(tabName) {
        var showOrHide = selected.attr('id') == tabName + '_tab';
        $('#' + tabName + '_tab').toggleClass('active', showOrHide);
        $('#' + tabName).toggle(showOrHide);
    });
}

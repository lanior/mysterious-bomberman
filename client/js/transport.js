function WebSocketTransport(url) {
    console.log('WS: connecting to ' + url);

    this.socket = new WebSocket(url);
    this.open = false;
    this.handlers = {};
    this.readyCallbacks = [];

    var transport = this;
    this.socket.onopen = function() {
        console.log('WS open');
        transport.open = true;
        _.each(transport.readyCallbacks, function(callback) {
            callback();
        });
    };

    this.socket.onclose = function() {
        console.log('WS close');
        transport.open = false;
    };

    this.socket.onerror =function(error) {
        console.log('WS error: ' + error);
        transport.open = false;
    };

    this.socket.onmessage = function(message) {
        if (!transport.open) {
            return;
        }

        try {
            var obj = JSON.parse(message.data);
        } catch (e) {
            console.log('WS message parse error', message.data, e);
            onWSClose();
            return;
        }
        // console.log('WS server event: ', obj);

        var handler = transport.handlers[obj.event];
        if (handler) {
            handler(obj);
        }
    };
}

WebSocketTransport.prototype.onReady = function(callback) {
    if (this.open) {
        setTimeout(callback, 0);
    } else {
        this.readyCallbacks.push(callback);
    }
};

WebSocketTransport.prototype.send = function(event, message) {
    message = message || {};
    message.event = event;
    this.socket.send(JSON.stringify(message));
    console.log('WS client event: ', message);
};

WebSocketTransport.prototype.close = function() {
    this.socket.close();
    this.open = false;
};

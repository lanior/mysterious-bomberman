var wallTypes = {
    name: 'Walls',
    values: {
        '#': 'unbreakable',
        '%': 'breakable'
    },
    availableAtLayers: [true, false, false]
};
var floorTypes = {
    name: 'Floor',
    values: {
        '.': 'empty',
        '>': 'bomb rolls right',
        '<': 'bomb rolls left',
        '^': 'bomb rolls up',
        'v': 'bomb rolls down',
        '*': 'black hole'
    },
    availableAtLayers: [true, true, true]
};
var bonusTypes = {
    name: 'Bonuses',
    values: {
        'b': 'bomb',
        'f': 'flame',
        'g': 'goldenflame',
        's': 'speed',
        'k': 'kicker',
        't': 'throw'
    },
    availableAtLayers: [true, true, false]
};
var diseaseTypes = {
    name: 'Diseases',
    values: {
        'i': 'invert',
        'c': 'lightspeed',
        'a': 'autobomb',
        'n': 'nobomb',
        'u': 'turtle'
    },
    availableAtLayers: [true, true, false]
};
var randomBonuses = {name: 'Random bonus', values: {'?': 'random'}, availableAtLayers: [false, true, false]};
var players = {name: 'Players', values: {'0': 'player position'}, availableAtLayers: [true, false, false]};

var tileTypes = [floorTypes, bonusTypes, diseaseTypes, wallTypes, players, randomBonuses];

var selectedTile;
var layers = [[], [], []];
var currentLayer = 0;
var mapViewRenderer, maskLayer, selectedTileRenderer = null;

function canChangeTile(cell_x, cell_y) {
    if(currentLayer == 0) {
        return selectedTile != '?';
    }
    if(selectedTile == null) {
        return false;
    }
    var prev = layers[currentLayer - 1][cell_y][cell_x],
        result =
            currentLayer == 1 && (
                isItem(selectedTile) && prev == '%' ||
                (selectedTile in floorTypes.values) && (prev == '%' || prev == '0' || isItem(prev))
            ) ||
            currentLayer == 2 && isItem(prev);
    return result;
}

function selectTile(key) {
    selectedTile = key;
    maskLayer.drawMask(canChangeTile);
    selectedTileRenderer.render([selectedTile]);
    return false;
}

function initTilePanel() {
    var container = $('#tiles_container');
    if(container.attr('loaded') == 'true') {
        return;
    }
    container.attr('loaded', 'true');

    var containerWidth = 0;
    _.each(tileTypes, function(tiles) {
        var tileGroup = $('<a>')
            .attr('href', '#')
            .text(tiles.name)
            .appendTo($('<li>').appendTo(container));
        var tileGroupContainer = $('<div>')
            .addClass('tile_group')
            .css({'position': 'relative', 'height': SPRITE_SIZE})
            .appendTo(tileGroup);

        function renderTile(canvas, key) {
            var mvr = new MapViewRenderer(canvas.getLayer(0));
            mvr.render([key]);
        }

        for(var key in tiles.values) {
            //hover -> tile.values[key]
            var c = $('<div>').css({'position': 'relative', 'float': 'left'}).appendTo(tileGroupContainer);
            var canvas = Canvas.createStatic(c, SPRITE_SIZE, SPRITE_SIZE);
            canvas.loadTextures(textures, _.bind(renderTile, null, canvas, key));
            c.click(_.bind(selectTile, null, key));
        }
    });
    container.css({'min-width': containerWidth * SPRITE_SIZE});
}

function fillLayers(height, width) {
    function getEmptyLine(size) {
        var emptyLine = '';
        for(var j = 0; j < size; j++) {
            emptyLine += '.';
        }
        return emptyLine;
    }

    if (_.size(layers[0]) == 0) {
        var defaultLine = getEmptyLine(width);
        for(var i = 0; i < height; i++) {
            _.each(layers, function(layer) { layer.push(defaultLine); });
        }
    } else {
        var prevHeight = _.size(layers[0]),
            prevWidth = _.size(layers[0][0]);

        if (height >= prevHeight) {
            var newLine = getEmptyLine(width);
            for(var i = 0; i < height - prevHeight; i++) {
                _.each(layers, function(layer) { layer.push(newLine); });
            }
        } else {
            _.each(layers, function(layer) { layer.length = height; });
        }

        if (width >= prevWidth) {
            var ending = getEmptyLine(width - prevWidth);
            for(var j = 0; j < 3; j++) {
                for(var i = 0; i < height; i++) {
                    layers[j][i] += ending;
                }
            }
        } else {
            for(var j = 0; j < 3; j++) {
                for(var i = 0; i < height; i++) {
                    layers[j][i] = layers[j][i].substring(0, width);
                }
            }
        }
    }
}

function disableNonAvailableBonuses() {
    $('#tiles_container a').each(function(id, menuGroup) {
        var tileGroup = _.find(tileTypes, function(type) { return type.name == $(menuGroup).text(); }),
            disabled = !tileGroup.availableAtLayers[currentLayer];
        $(menuGroup).parent().toggleClass('disabled', disabled);
        $(menuGroup).children().toggle(!disabled);
    });
}

function initPaginator() {
    if($('#map_layers').attr('loaded') == 'true') {
        return;
    }
    $('#map_layers').attr('loaded', 'true');

    var pages = $('#map_layers a');
    pages.click(function() {
        pages.parent().removeClass('active');
        $(this).parent().addClass('active');
        currentLayer = parseInt($(this).text()) - 1;

        disableNonAvailableBonuses();

        if (typeof mapViewRenderer != 'undefined') {
            mapViewRenderer.render(layers[currentLayer]);
            maskLayer.drawMask(canChangeTile);
        }
        return false;
    });
    disableNonAvailableBonuses();
}

function isItem(element) {
    return element in bonusTypes.values || element in diseaseTypes.values || element == '?';
}

function compileMapFromLayers() {
    var height = _.size(layers[0]),
        width = _.size(layers[0][0]),
        result = [],
        playerCount = 0;

    for(var i = 0; i < height; i++) {
        result.push('');
        for(var j = 0; j < width; j++) {
            if(layers[0][i][j] == '%' && isItem(layers[1][i][j])) {
                if(layers[1][i][j] == '?') {
                    result[i] += '%';
                    result[i] += '%';
                } else {
                    result[i] += layers[1][i][j].toUpperCase();
                    result[i] += layers[2][i][j];
                }
            } else {
                if(layers[0][i][j] == '0') {
                    result[i] += playerCount.toString();
                    playerCount++;
                } else {
                    result[i] += layers[0][i][j];
                }
                result[i] += layers[1][i][j];
            }
        }
    }
    return result;
}

function initMapField() {
    function tryChangeCellType(cell_x, cell_y) {
        if (selectedTile == undefined || !canChangeTile(cell_x, cell_y)) {
            return false;
        }
        if (selectedTile == layers[currentLayer][cell_y][cell_x]) {
            return false;
        }

        var limit = 2,
            thinCell = selectedTile == '0' || selectedTile == '%' || isItem(selectedTile);

        if(thinCell) {
            limit = currentLayer;
        }

        var fillDeepLayers = function(start, end, element) {
            for(var i = start; i <= end; i++) {
                var line = layers[i][cell_y];
                layers[i][cell_y] = line.substr(0, cell_x) + element;
                if(line.length != cell_x + 1) {
                    layers[i][cell_y] += line.substring(cell_x + 1);
                }
            }
        }
        fillDeepLayers(currentLayer, limit, selectedTile);

        if(thinCell && currentLayer != 2 && layers[currentLayer + 1][cell_y][cell_x] == '#') {
            fillDeepLayers(currentLayer + 1, 2, '.')
        }
        mapViewRenderer.render(layers[currentLayer]);
    }

    var mapContainer = $('#map_container');
    mapContainer
        .unbind()
        .mousedown(function(e) {
            if(selectedTile == undefined) {
                return false;
            }
            mapContainer.mousemove(function(e) {
                var cell_x = Math.floor((e.pageX - this.offsetLeft) / SPRITE_SIZE),
                    cell_y = Math.floor((e.pageY - this.offsetTop) / SPRITE_SIZE);

                tryChangeCellType(cell_x, cell_y);
            });
        })
        .mouseup(function(e) {
            mapContainer.unbind('mousemove');
        })
        .click(function(e) {
            var cell_x = Math.floor((e.pageX - this.offsetLeft) / SPRITE_SIZE),
                cell_y = Math.floor((e.pageY - this.offsetTop) / SPRITE_SIZE);

            tryChangeCellType(cell_x, cell_y);
            return false;
        });
}

$(document).ready(function () {
    var widthField = $('#map_width'),
        heightField = $('#map_height');

    $('#resize_field').click(function() {
        var width = parseInt(widthField.val()),
            height = parseInt(heightField.val());

        if (isNaN(width) || isNaN(height)) {
            alert('Map sizes should be numbers');
            return false;
        } else if (
            width < config.minMapSize ||
            width > config.maxMapSize ||
            height < config.minMapSize ||
            height > config.maxMapSize
        ) {
            alert(
                'Map sizes should be numbers between ' +
                config.minMapSize.toString() + ' and ' +
                config.maxMapSize.toString()
            );
            return false;
        }
        fillLayers(height, width);

        var mapContainer = $('#map_container').empty();
        var canvas = Canvas.createStatic(mapContainer, width * SPRITE_SIZE, height * SPRITE_SIZE);
        canvas.loadTextures(textures, function () {
            mapViewRenderer = new MapViewRenderer(canvas.getLayer(0));
            mapViewRenderer.render(layers[currentLayer]);
            maskLayer = canvas.getLayer(1);
            maskLayer.drawMask(canChangeTile);
        });
        initMapField();
        return false;
    });

    $('#new_map').click(function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        setUserState(userStateUploadMap);

        $('#upload_map_form input[name=map_name]').val('');
        widthField.attr('min', config.minMapSize).attr('max', config.maxMapSize).attr('value', 20);
        heightField.attr('min', config.minMapSize).attr('max', config.maxMapSize).attr('value', 20);

        initTilePanel();
        initPaginator();
        createSettingsField('upload_map_settings', 'upload_map_items', config.settings);

        layers = [[], [], []];
        currentLayer = 0;
        $('#resize_field').click();
        $('#upload_map_field_tab').click();
        $('#map_layer_1').click();

        if (selectedTileRenderer == null) {
            var canvas = Canvas.createStatic($('#selected_tile'), SPRITE_SIZE, SPRITE_SIZE);
            canvas.loadTextures(textures, _.bind(function(canvas) {
                selectedTileRenderer = new MapViewRenderer(canvas.getLayer(0));
                selectTile('.');
            }, null, canvas));
        } else {
            selectTile('.');
        }

        return false;
    });

    $('#upload_map').click(function() {
        api.upload_map({
            name: $('#upload_map_form input[name=map_name]').val(),
            field: compileMapFromLayers(),
            settings: collectSettings()
        }, function(data) {
            if(data['status'] == 'ok') {
                //action
                setUserState(userStateLoggedIn);
            } else {
                alert(data['status']);
            }
        });
        return false;
    });

    var uploadMapTabNames = ['upload_map_field', 'upload_map_settings', 'upload_map_items'];
    $('#upload_map_field_tab').click(function() {
        selectTab(uploadMapTabNames, $(this));
        return false;
    });
    $('#upload_map_settings_tab').click(function() {
        selectTab(uploadMapTabNames, $(this));
        return false;
    });
    $('#upload_map_items_tab').click(function() {
        selectTab(uploadMapTabNames, $(this));
        return false;
    });
});

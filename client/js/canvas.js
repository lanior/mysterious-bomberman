var Canvas = {
    create: function(container, width, height) {
        return new CanvasWrapper(container, width, height);
    },

    createStatic: function(container, width, height) {
        return new CanvasWrapper(container, width, height);
    }
};

function CanvasWrapper(container, width, height) {
    this.container = container;
    this.container.css('position', 'relative');

    this._scale = 1;
    this._tx = 0;
    this._ty = 0;

    this.layers = [];
    this.width = width;
    this.height = height;
    this.resize(width, height);
}

CanvasWrapper.prototype.resize = function(width, height) {
    this.container.css({
        width: width + 'px',
        height: height + 'px'
    });

    _.each(this.layers, function(layer) {
        layer._resize(width, height);
    });
};

CanvasWrapper.prototype.loadTextures = function(textures, callback) {
    var counter = _.size(textures);
    var loadCallback = function() {
        counter--;
        if (counter === 0) {
            callback();
        }
    };

    _.each(textures, function(texture) {
        if (typeof texture.canvasImage == 'undefined') {
            texture.canvasImage = new Image();
            texture.canvasImage.onload = loadCallback;
            texture.canvasImage.src = texture.url;
        } else {
            setTimeout(loadCallback, 0);
        }
    });
};

CanvasWrapper.prototype.getLayer = function(z) {
    var layer = new CanvasLayer(this, z);
    this.layers.push(layer);
    return layer;
};

CanvasWrapper.prototype.clear = function() {
    _.invoke(this.layers, 'clear');
};

CanvasWrapper.prototype.scale = function(scale) {
    this._scale = scale;
    _.invoke(this.layers, 'scale', scale);
};

CanvasWrapper.prototype.translate = function(tx, ty) {
    this._tx = tx;
    this._ty = ty;
    _.invoke(this.layers, 'translate', tx, ty);
};

CanvasWrapper.prototype.show = function() {
    this.container.show();
};

CanvasWrapper.prototype.hide = function() {
    this.container.hide();
};

function CanvasLayer(canvasRenderer, z) {
    this.el = $('<canvas>')
        .css({
            zIndex: z,
            position: 'absolute',
            top: 0,
            left: 0
        })
        .appendTo(canvasRenderer.container)[0];
    this.context = this.el.getContext('2d');
    this._resize(canvasRenderer.width, canvasRenderer.height);
}

CanvasLayer.prototype.drawMask = function(maskFunc) {
    this.clear();
    var fs = this.context.fillStyle;
    this.context.fillStyle = 'rgba(255, 255, 255, 0.5)';
    for (var i = 0; i < this.el.width / SPRITE_SIZE; i++) {
        for (var j = 0; j < this.el.height / SPRITE_SIZE; j++) {
            if (!maskFunc(i, j)) {
                this.context.fillRect(i * SPRITE_SIZE, j * SPRITE_SIZE, SPRITE_SIZE, SPRITE_SIZE);
            }
        }
    }
    this.context.fillStyle = fs;
}

CanvasLayer.prototype.drawSprite = function(sprite, x, y) {
    this.context.drawImage(
        sprite.texture.canvasImage,
        sprite.x,
        sprite.y,
        sprite.width,
        sprite.height,
        parseInt(x * scale, 10),
        parseInt(y * scale, 10),
        sprite.width,
        sprite.height
    );
};

CanvasLayer.prototype._resize = function(width, height) {
    if (this.el.width != width || this.el.height != height) {
        this.el.width = width;
        this.el.height = height;
    }
};

CanvasLayer.prototype.clear = function() {
    this.context.clearRect(0, 0, this.el.width, this.el.height);
};

CanvasLayer.prototype.scale = function(scale) {
    this.context.scale(scale, scale);
};

CanvasLayer.prototype.translate = function(x, y) {
    this.context.translate(x, y);
};

function Texture(url) {
    this.url = url;
}

var SPRITE_SIZE = 40;
function Sprite(texture, x, y) {
    this.texture = texture;
    this.x = x * SPRITE_SIZE;
    this.y = y * SPRITE_SIZE;
    this.width = SPRITE_SIZE;
    this.height = SPRITE_SIZE;
}

Sprite.prototype.draw = function(canvas, x, y) {
    canvas.drawSprite(this, x, y);
};

function CompositeSprite(sprites) {
    this.sprites = sprites;
}

CompositeSprite.prototype.draw = function(canvas, x, y) {
    _.each(this.sprites, function(sprite) {
        canvas.drawSprite(sprite, x, y);
    });
};

function AnimatedSprite(animations) {
    this.animations = animations;
}

AnimatedSprite.prototype.draw = function(canvas, x, y, animation, time) {
    this.drawEx(canvas, x, y, animation, time, this.animations[animation][0]);
};

AnimatedSprite.prototype.drawEx = function(canvas, x, y, animation, time, duration) {
    var data = this.animations[animation],
        sprites = data[1],
        sprite = sprites[parseInt((time % duration) / duration * sprites.length, 10)];
    sprite.draw(canvas, x, y);
};

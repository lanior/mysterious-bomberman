var cellSize = 10000;
var maxSlideLength = 5000;
var ticksPerSecond = 30;
var tickPeriod = parseInt(1000 / ticksPerSecond);
var scale = 1 / cellSize * SPRITE_SIZE;

var movePlayer = (function() {
    var cellFor = function(pos) {
        return new Vector2(
            Math.floor((pos.x + cellSize / 2) / cellSize),
            Math.floor((pos.y + cellSize / 2) / cellSize)
        );
    };

    var getObstacles = function(pos, state) {
        var x1 = pos.x < 0 ? -1 : Math.floor(pos.x / cellSize)
            y1 = pos.y < 0 ? -1 : Math.floor(pos.y / cellSize),
            x2 = Math.floor((pos.x + cellSize - 1) / cellSize),
            y2 = Math.floor((pos.y + cellSize - 1) / cellSize);

        var obstacles = [];
        for (var x = x1; x <= x2; x++) {
            for (var y = y1; y <= y2; y++) {
                var cellHasBomb = false;
                _.each(state.bombs, function(bomb) {
                    var cell = cellFor(bomb);
                    if (cell.x == x && cell.y == y)
                        cellHasBomb = true;
                });

                var cell = '#';
                if (x >= 0 && x < state.map.width && y >= 0 && y < state.map.height) {
                    cell = state.map.field[y][x];
                }

                if (cellHasBomb || cell == '$' || cell == '%' || cell == '#') {
                    obstacles.push(new Vector2(x, y).mul(cellSize));
                }
            }
        }
        return obstacles;
    };

    var vdifference = function(a, b) {
        return _.reject(a, function(el) {
            return _.size(_.where(b, {x: el.x, y: el.y})) > 0;
        });
    };

    var getDistance = function(v1, v2) {
        return Math.abs(v1.x - v2.x) + Math.abs(v1.y - v2.y);
    };

    var getIntersectionLength = function(a, b) {
        return Math.max(0, cellSize - Math.abs(a.x - b.x))
             + Math.max(0, cellSize - Math.abs(a.y - b.y));
    };

    return function(player, state, dt) {
        var oldPos = new Vector2(player.x, player.y),
            distance = player.speed * dt,
            direction = Vector2.getDirection(player.movement_direction),
            pos = oldPos.add(direction.mul(distance)),
            oldObstacles = getObstacles(oldPos, state),
            obstacles = vdifference(getObstacles(pos, state), oldObstacles);

        if (obstacles.length > 0) {
            switch (player.movement_direction) {
            case 'left':  pos.x = obstacles[0].x + cellSize; break;
            case 'right': pos.x = obstacles[0].x - cellSize; break;
            case 'up':    pos.y = obstacles[0].y + cellSize; break;
            case 'down':  pos.y = obstacles[0].y - cellSize; break;
            }
        }

        var remainDistance = distance - getDistance(oldPos, pos);
        if (obstacles.length == 1 && remainDistance > 0) {
            var slideVector = pos.sub(obstacles[0]).sign().add(direction),
                intersectionLength = getIntersectionLength(pos, obstacles[0]);

            if (intersectionLength <= maxSlideLength) {
                var slideLength = Math.min(intersectionLength, remainDistance);
                pos = pos
                    .add(slideVector.mul(slideLength))
                    .add(direction.mul(remainDistance - slideLength));
            }
        }
        return pos;
    };
})();

function Map(field) {
    this.field = field;
    this.height = field.length;
    this.width = (field[0] || []).length;
}

Map.prototype.getVisualWidth = function() {
    return this.width * SPRITE_SIZE;
}

Map.prototype.getVisualHeight = function() {
    return this.height * SPRITE_SIZE;
}

function MapViewRenderer(layout) {
    this.currentField = [];
    this.layout = layout;
}

MapViewRenderer.prototype.render = function(field) {
    if (this.currentField.length == 0) {
        for (var y = 0; y < field.length; y++) {
            this.currentField.push('');
        }
    }

    for (var y = 0; y < field.length; y++) {
        var line = field[y], oldLine = this.currentField[y];
        if (oldLine != line) {
            for (var x = 0; x < line.length; x++) {
                if (oldLine[x] != line[x]) {
                    var tile = sprites.tiles[field[y][x]] || sprites.unknownTile;
                    tile.draw(this.layout, x * cellSize, y * cellSize);
                }
            }
            this.currentField[y] = line;
        }
    }
};

MapViewRenderer.prototype.clear = function() {
    this.currentField = [];
    this.layout.clear();
};

function GameState(state) {
    this.map = new Map(state.field);
    this._prepare(state);
}

GameState.prototype._prepare = function(state) {
    _.each(state.bombs, function(bomb) {
        bomb.created_at *= tickPeriod;
        bomb.destroy_at *= tickPeriod;
        bomb.speed /= tickPeriod;
    });

    _.each(state.explosions, function(explosion) {
        explosion.created_at *= tickPeriod;
        explosion.destroy_at *= tickPeriod;
    });

    _.each(state.players, function(player) {
        player.direction_changed_at *= tickPeriod;
        player.speed /= tickPeriod;
    });

    this.tick = state.tick;
    this.players = state.players;
    this.bombs = state.bombs;
    this.explosions = state.explosions;
};

function GameRenderer(canvas) {
    this.mapViewRenderer = new MapViewRenderer(canvas.getLayer(0));
    this.actorsLayout = canvas.getLayer(1);
    this.currentField = [];
}

GameRenderer.prototype.renderBombs = function(bombs, elapsed, dt) {
    var layout = this.actorsLayout;
    _.each(bombs, function(bomb) {
        var lifeTime = elapsed - bomb.created_at,
            direction = Vector2.getDirection(bomb.movement_direction),
            pos = direction.mul(dt).add(bomb);
        sprites.bomb.draw(layout, pos.x, pos.y, 'stand', lifeTime);
    });
};

GameRenderer.prototype.renderExplosions = function(explosions, elapsed, dt) {
    var explosionsAnimationMap = {
        'left':  'horizontal',
        'right': 'horizontal',
        'up':    'vertical',
        'down':  'vertical',
    };

    var layout = this.actorsLayout;
    _.each(explosions, function(explosion) {
        var lifeTime = elapsed - explosion.created_at,
            maxLifeTime = explosion.destroy_at - explosion.created_at;

        if (lifeTime < maxLifeTime) {
            sprites.explosion.drawEx(layout, explosion.x, explosion.y, 'center', lifeTime, maxLifeTime);
            _.each(Vector2.directions, function(unitVector, direction) {
                var length = explosion[direction + '_length'];
                for (var i = 1; i <= length; i++) {
                    var animation = i == length ? direction : explosionsAnimationMap[direction],
                        pos = unitVector.mul(i * cellSize).add(explosion);
                    sprites.explosion.drawEx(layout, pos.x, pos.y, animation, lifeTime, maxLifeTime);
                }
            });
        }
    });
};

GameRenderer.prototype.renderPlayers = function(state, elapsed, dt) {
    var layout = this.actorsLayout;
    var playerPositions = [];
    _.each(state.players, function(player, id) {
        var movementTime = elapsed - player.direction_changed_at,
            pos = movePlayer(player, state, dt);

        playerPositions.push(pos);
        if (id == currentPlayerIndex) {
            currentPos = pos.copy();
        }

        var animation;
        if (player.dead) {
            animation = 'dead_' + player.look_direction;
        } else if (player.movement_direction != 'none') {
            animation = 'move_' + player.movement_direction;
        } else {
            animation = 'stand_' + player.look_direction;
        }
        sprites.player.draw(layout, pos.x, pos.y, animation, movementTime);
    });
    return playerPositions;
};

GameRenderer.prototype.renderGameState = function(state, elapsed) {
    var dt = elapsed - state.tick * tickPeriod;
    this.actorsLayout.clear();
    this.mapViewRenderer.render(state.map.field);
    this.renderBombs(state.bombs, elapsed, dt);
    this.renderExplosions(state.explosions, elapsed, dt);
    return this.renderPlayers(state, elapsed, dt);
};

GameRenderer.prototype.clear = function() {
    this.mapViewRenderer.clear();
    this.actorsLayout.clear();
};

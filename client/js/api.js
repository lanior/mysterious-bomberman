var serverAddress = null;
var api = (function () {
    var api = {};

    var basicMethods = [
        'extensions',
        'login',
        'register'
    ];

    var userMethods = [
        'create_game',
        'get_map',
        'player_state',
        'join_game',
        'leave_game',
        'list_games',
        'list_maps',
        'logout',
        'send_message',
        'start_game',
        'upload_map'
    ];

    var requestId = 0;
    var sendRequest = function(action, useSid, request, callback) {
        request.action = action;
        if (useSid) {
            request.sid = api.sid;
        }

        var id = ++requestId;
        console.log('Request #' + id + ': ', request);
        $.ajax(serverAddress, {
            dataType: 'jsonp',
            type: 'GET',
            data: {'q': JSON.stringify(request)},
            success: function (data) {
                console.log('Response #' + id + ': ', data);
                callback(data);
            }
        });
    };

    _.each(basicMethods, function (action) {
        api[action] = _.bind(sendRequest, null, action, false);
    });

    _.each(userMethods, function (action) {
        api[action] = _.bind(sendRequest, null, action, true);
    });
    return api;
})();

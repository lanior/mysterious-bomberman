var config = null;
var gameId = null;
var gameStarted = false;
var currentPlayerIndex;
var currentPos;
var gameFieldSize = new Vector2(500, 400);

function Configuration(min, max, settings, url) {
    this.minMapSize = min;
    this.maxMapSize = max;
    this.settings = settings;
    this.websocketUrl = url;
}

api.sid = $.cookie('bomberman_sid');

var eventHandlers = {
    auth_success: function() {},
    auth_error: function() {},
    new_message: function(data) {
        var newMessage = $('<li>')
            .attr('timestamp', data['timestamp'])
            .text(data['username'] + ': ' + data['text']);

        if($('#messages li').length != 0) {
            $('#messages li').each(function(id, message) {
                if(parseInt($(message).attr('timestamp')) <= parseInt(data['timestamp'])) {
                    newMessage.insertBefore(message);
                    return false;
                }
            });
        } else {
            newMessage.appendTo($('#messages'));
        }
    },

    player_joined_game: function(data) {
        $('<li>')
            .attr('id', 'player_' + data['username'])
            .text(data['username'])
            .appendTo($('#list_players'));
    },

    player_left_game: function(data) {
        $('#player_' + data['username']).remove();
    },

    vote_start: function(data) {
        $('<i>')
            .addClass('icon-ok')
            .appendTo($('#player_' + data['username']));
        if (data['username'] == $.cookie('bomberman_username')) {
            setUserState(userStateWaiting);
        }
    },

    game_started: function(data) {
        setUserState(userStateGameStarted);
        currentPlayerIndex = data['current_player_index'];
        if (gameEnv != null)
            gameEnv.initPlayerLabels(data['players'], currentPlayerIndex);
    },

    game_state: function(state) {
        if (gameEnv != null)
            gameEnv.setState(state);
    },

    protocol_violation: function() {
        console.log('protocol violation');
    },

    game_end: function(data) {
        console.log(data);
        alert('Game End! ' + (data.winner ? ('Winner: ' + data.winner) : 'Draw'));
        $('#leave_game').click();
    }
};

function setPrimaryState() {
    api.player_state({}, function(data) {
        switch(data['status']) {
        case 'ok':
            if(data['state'] == 'not_in_game') {
                setUserState(userStateLoggedIn);
            } else {
                joinGame(data['game_id'], data['game_name']);
                if(data['state'] == 'in_started_game') {
                    setUserState(userStateGameStarted);
                    gameStarted = true;
                } else {
                    setUserState(userStateInGame);
                }
            }
            break;
        default:
            alert(data['status']);
        }
    });
}

function initForUser() {
    transport = new WebSocketTransport(config.websocketUrl);
    transport.handlers = eventHandlers;
    transport.onReady(function() {
        transport.send('auth', { sid: api.sid });
    });
    setPrimaryState();
    loadGames();
}

function connectToServer(address, callback) {
    $.cookie('server_address', address);
    serverAddress = address;

    api.extensions({}, function(data) {
        if(data['status'] == 'ok') {
            var coreExt = data['extensions']['core.v0'];
            if (typeof coreExt != 'undefined') {
                config = new Configuration(
                    coreExt.min_map_size,
                    coreExt.max_map_size,
                    coreExt.default_game_settings,
                    coreExt.websocket_url
                );
                callback();
            } else {
                alert('core.v0 extension not supported');
            }
        } else {
            alert(data['status']);
        }
    });

    $('#connected').show();
    $('#select_server').hide();
}

$(document).ready(function () {
    $('#server_address').val('http://' + location.host + '/api/');
    $('#game_field').css({'max-width': gameFieldSize.x, 'max-height': gameFieldSize.y});
    gameCanvas = Canvas.create($('#game_field'), 100, 100);
    gameCanvas.loadTextures(textures, function() {
        gameRenderer = new GameRenderer(gameCanvas);
        if ($.cookie('server_address') != null) {
            connectToServer(
                $.cookie('server_address'),
                function() {
                    if (api.sid != null) {
                        initForUser();
                    }
                }
            );
        } else {
            $('#change_server').click();
        }
    });

    var usernameField = $('#login_form input[name=username]'),
        passwordField = $('#login_form input[name=password]');

    $('#login').click(function() {
        api.login({
            username: usernameField.val(),
            password: passwordField.val()
        }, function (data) {
            switch(data['status']) {
            case 'ok':
                api.sid = data['sid'];
                $.cookie('bomberman_sid', api.sid, { expires: 7 });
                $.cookie('bomberman_username', usernameField.val(), { expires: 7 });

                usernameField.val('');
                passwordField.val('');

                initForUser();
                break;
            case 'invalid_credentials':
                alert('Invalid credentials');
                break;
            case 'malformed_request':
                alert('Malformed request');
                break;
            }
        });
        return false;
    });

    $('#register').click(function() {
        api.register({username: usernameField.val(), password: passwordField.val()}, function (data) {
            switch(data['status']) {
            case 'ok':
                $('#login').click();
                break;
            case 'malformed_request':
                alert('Malformed request');
                break;
            case 'user_already_exists':
                alert('User with login ' + usernameField.val() + ' already exists');
                break;
            }
        });
        return false;
    });

    $('#logout').click(function() {
        if(gameId != null) {
            api.leave_game({}, function(data) {});
        }
        api.sid = null;
        $.removeCookie('bomberman_sid');
        $.removeCookie('bomberman_username');
        if(typeof transport != 'undefined') {
            transport.close();
        }

        setUserState(userStateAnonymous);
        return false;
    });

    $('#change_server').click(function() {
        $('#logout').click();
        $('#connected').hide();
        $('#select_server').show();
        $.removeCookie('server_address');
        return false;
    });

    $('#connect').click(function() {
        var serverAddressField = $('#server_address');
        connectToServer(serverAddressField.val(), function() {});
        serverAddressField.val('');
        return false;
    });

    $('#create_game_settings, #create_game_items').hide();
});

window.onunload = function () {
    if(typeof transport != 'undefined')
        transport.close();
}

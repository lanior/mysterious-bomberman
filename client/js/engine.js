var DEFAULT_CONTROLS = {
    13: 'action',
    37: 'left',
    38: 'up',
    39: 'right',
    40: 'down'
};

function InputController(transport, controls) {
    this.transport = transport;
    this.controls = controls;
    this.keysStack = [];
    this.action = 'none';

    var onKeyDown = _.bind(this.onKeyDown, this),
        onKeyUp = _.bind(this.onKeyUp, this);

    this.attach = function(target) {
        target.on('keydown', onKeyDown);
        target.on('keyup', onKeyUp);
    };

    this.detach = function(target) {
        target.off('keydown', onKeyDown);
        target.off('keyup', onKeyUp);
    };
}

InputController.prototype.sendCommand = function() {
    this.transport.send('game_action', {
        direction: this.keysStack[this.keysStack.length - 1] || 'none',
        action: this.action
    });
};

InputController.prototype.onKeyDown = function(event) {
    var action = this.controls[event.keyCode];
    if (!action) {
        return;
    }

    if (action == 'action') {
        this.action = 'place_bomb';
        this.sendCommand();
    } else if (this.keysStack.indexOf(action) == -1) {
        this.keysStack.push(action);
        this.sendCommand();
    }
    return false;
};

InputController.prototype.onKeyUp = function(event) {
    var action = this.controls[event.keyCode];
    var index = this.keysStack.indexOf(action);
    if (index != -1) {
        this.keysStack.splice(index, 1);
        this.sendCommand();
    } else if (action == 'action') {
        this.action = 'none';
        this.sendCommand();
    }
};

var gameFieldSize = new Vector2(800, 600);

function randomString() {
    var str = '';
    var a = 'a'.charCodeAt(0),
        z = 'z'.charCodeAt(0);
    for (var i = 0; i < 10; i++) {
        str += String.fromCharCode(a + parseInt((z - a + 1) * Math.random(), 10));
    }
    return str;
}

function getUserSid(callback) {
    var username = randomString();
    api.register({ username: username, password: 'asd' }, function() {
        api.login({ username: username, password: 'asd' }, function (data) {
            callback(data.sid);
        });
    });
}

var testMap = [
    "##################################",
    "##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%##",
    "##%%%%0.%%%%%%%%%%%%%%%%%%3.%%%%##",
    "##%%%%......%%%%%%%%%%......%%%%##",
    "##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%##",
    "##%%%%%%%%%%%%%%##%%%%%%%%%%%%%%##",
    "##%%%%%%%%%%%%######%%%%%%%%%%%%##",
    "##%%%%%%%%%%%%%%##%%%%%%%%%%%%%%##",
    "##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%##",
    "##%%%%......%%%%%%%%%%......%%%%##",
    "##%%%%2.%%%%%%%%%%%%%%%%%%1.%%%%##",
    "##%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%##",
    "##################################"
];

var defaultSettings;
var currentPlayerIndex;
serverAddress = '/api/';

var transport;
var mr, from, lastState, renderer;

var sid1, sid2, ge;

var eventHandlers = {
    auth_success: function() {
        api.upload_map({
            name: 'test map',
            settings: defaultSettings,
            field: testMap
        }, function(data) {
            api.create_game({
                name: 'test_game',
                password: '',
                map_id: data.map_id,
                max_player_count: 2,
                settings: defaultSettings
            }, function (data) {
                var gameId = data.game_id;
                api.sid = sid2;
                api.join_game({ game_id: gameId, password: '' }, function () {
                    api.start_game({}, function () {
                        api.sid = sid1;
                        api.start_game({}, function () {});
                    })
                });
            });
        });
    },

    game_started: function(data) {
        currentPlayerIndex = data['current_player_index'];
        ge.initPlayerLabels(data.players, currentPlayerIndex);
    },

    game_state: function(state) {
        ge.setState(state);
    },

    protocol_violation: function() {
        console.log('protocol violation');
    },

    game_end: function(data) {
        console.log(data)
        alert('Game End! ' + (data.winner ? ('Winner: ' + data.winner) : 'Draw'))
    }
};

$(function () {
    var c = $('<div>').css({
        backgroundColor: 'blue',
        marginLeft: '30px'
    }).appendTo('body');

    $('div').not(c).hide();

    renderer = Canvas.create(c, 100, 100);
    renderer.loadTextures(textures, function () {
        mr = new GameRenderer(renderer);
        api.extensions({}, function(data) {
            var core = data.extensions['core.v0'];
            var url = core.websocket_url;
            defaultSettings = core.default_game_settings;

            getUserSid(function(sid) {
                sid1 = sid;
                api.sid = sid;
                getUserSid(function(ssid2) {
                    sid2 = ssid2;
                    transport = new WebSocketTransport(url);
                    transport.handlers = eventHandlers;

                    ge = new GameEnvironment(transport, renderer, mr);
                    ge.render();

                    transport2 = new WebSocketTransport(url);
                    transport2.onReady(function() {

                        var ic2 = new InputController(transport2, {
                            9: 'action',
                            65: 'left',
                            87: 'up',
                            68: 'right',
                            83: 'down'
                        });
                        ic2.attach($(document));

                        transport2.handlers = {
                            auth_success: function () {
                                transport.onReady(function() {
                                    transport.send('auth', { sid: sid });
                                });
                            }
                        }
                        transport2.send('auth', { sid: sid2 });
                    });
                });
            });
        });
    });
});

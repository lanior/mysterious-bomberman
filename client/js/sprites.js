var textures = {
    bombs: new Texture('img/bombs.png'),
    bomberSpider: new Texture('img/bomber_spider.png'),
    explosions: new Texture('img/explosions.png'),
    items: new Texture('img/extras.png'),
    tileAddons: new Texture('img/maptile_addons.png'),
    tiles: new Texture('img/maptiles.png')
};

var sprites = (function() {
    var tileSet = 1;
    var floorSprite = new Sprite(textures.tiles, tileSet * 4 + 0, 0);
    var tiles = {
        '.': floorSprite,
        '#': new Sprite(textures.tiles, tileSet * 4 + 1, 0),
        '%': new Sprite(textures.tiles, tileSet * 4 + 2, 0),
        '$': new Sprite(textures.tiles, tileSet * 4 + 3, 0),

        'v': new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 1, 0)]),
        '<': new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 2, 0)]),
        '^': new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 3, 0)]),
        '>': new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 4, 0)]),
        '*': new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 7, 0)]),

        'b': new Sprite(textures.items, 0, 0),
        's': new Sprite(textures.items, 2, 0),
        'f': new Sprite(textures.items, 1, 0),
        'g': new Sprite(textures.items, 8, 0),
        'k': new Sprite(textures.items, 3, 0),
        't': new Sprite(textures.items, 4, 0),

        'i': new Sprite(textures.items, 5, 0),
        'c': new Sprite(textures.items, 10, 0),
        'a': new Sprite(textures.items, 6, 0),
        'n': new Sprite(textures.items, 9, 0),
        'u': new Sprite(textures.items, 7, 0)
    };

    for (var i = 0; i < 10; i++) {
        tiles[i] = new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 6, 0)]);
    }

    var unknownTile = new CompositeSprite([floorSprite, new Sprite(textures.tileAddons, 5, 0)]);
    var bombSprite = new AnimatedSprite({
        'stand': [400, [
            new Sprite(textures.bombs, 0, 1),
            new Sprite(textures.bombs, 1, 1),
            new Sprite(textures.bombs, 2, 1),
            new Sprite(textures.bombs, 3, 1)
        ]]
    });

    var explosionSprite = (function() {
        var col = function(x) {
            return [1000, [
                new Sprite(textures.explosions, x, 2),
                new Sprite(textures.explosions, x, 1),
                new Sprite(textures.explosions, x, 0),
                new Sprite(textures.explosions, x, 1),
                new Sprite(textures.explosions, x, 2)
            ]];
        };
        return new AnimatedSprite({
            'center':     col(6),
            'horizontal': col(1),
            'vertical':   col(4),
            'left':       col(0),
            'right':      col(2),
            'up':         col(3),
            'down':       col(5)
        });
    })();

    var spiderSprite = (function(texture) {
        var animations = {};
        var directions = ['down', 'left', 'up', 'right'];
        _.each(directions, function(direction, y) {
            animations['stand_' + direction] = [1, [new Sprite(texture, 0, y)]];
            animations['dead_' + direction] = [1, [new Sprite(texture, 9, y)]];

            var movement = [];
            for (var i = 0; i <= 8; i++) {
                movement.push(new Sprite(texture, i, y));
            }
            animations['move_' + direction] = [500, movement];
        });
        return new AnimatedSprite(animations);
    })(textures.bomberSpider);

    return {
        tiles: tiles,
        unknownTile: unknownTile,
        bomb: bombSprite,
        explosion: explosionSprite,
        player: spiderSprite
    };
})();

function joinGame(joinGameId, joinGameName, mapId) {
    gameId = joinGameId;
    $('#current_game').text('You have joined to game ' + joinGameName);

    if (typeof mapId != 'undefined') {
        api.get_map({ map_id: mapId }, function(data) {
            if(data['status'] == 'ok') {
                createMapPreview(data.field, $('#game_map_preview'));
            }
        });
    }
}

function loadGames() {
    api.list_games({}, function(data) {
        if(data['status'] == 'ok') {
            $('#list_games .game_data').remove();

            _.each(data['games'], function(game) {
                var hasPassword = game['has_password'],
                    total = game['total_slots'],
                    free = game['free_slots'],
                    gameElement = $('<li>')
                        .addClass('game_data')
                        .appendTo($('#list_games'));

                var link = $('<a>')
                    .addClass('select_game')
                    .appendTo(gameElement);

                link.click(function() {
                    var gameName = game['name'],
                        gamePasswordField = $('#join_game_form input[name=game_password]');
                    //toggle active class?
                    setUserState(userStatePreviewGame);
                    $('#previewed_game').text('Game ' + gameName);

                    gamePasswordField.toggle(hasPassword);
                    $('#join_game')
                        .attr('game_id', game['id'])
                        .attr('game_name', gameName);
                    return false;
                });

                if(hasPassword) {
                    $('<i>').addClass('icon-lock').appendTo(link);
                }

                $('<span>')
                    .addClass(hasPassword ? 'locked' : 'unlocked')
                    .text(game['name'] + ' (' + (total - free).toString() + '/' + total + ')')
                    .appendTo(link);
            });
        } else {
            alert('Malformed request');
        }
    });
}

function prepareMapField(field) {
    var rows = [];
    for (var i = 0; i < field.length; i++) {
        var line = '';
        for (var j = 0; j < field[i].length; j += 2) {
            line += field[i][j] >= 'A' && field[i][j] <= 'Z' ? '%' : field[i][j];
        }
        rows.push(line);
    }
    return rows;
}

function createMapPreview(field, el) {
    var canvasEl = $('.canvas').empty();
    var mapCanvas = Canvas.create(canvasEl, 100, 100);
    var map = new Map(prepareMapField(field));
    var mapRenderer = new MapViewRenderer(mapCanvas.getLayer(0));

    var scale = Math.min(1, el.width() / map.getVisualWidth(), el.height() / map.getVisualHeight());

    mapCanvas.resize(map.getVisualWidth() * scale, map.getVisualHeight() * scale);
    mapCanvas.scale(scale);
    mapRenderer.render(map.field);
}

function selectMap(gameMap) {
    $('#map_name')
        .attr('map_id', gameMap.map_id)
        .text(gameMap.name);

    api.get_map({map_id: gameMap.map_id}, function(data) {
        if(data['status'] == 'ok') {
            createMapPreview(data.field, $('#create_game_map_preview'));
            createSettingsField('create_game_settings', 'create_game_items', data.settings);
            $('#create_game_container li').removeClass('disabled');
            $('#create_game_basic_tab').click();
            $('#max_player_count').attr('max', data.max_player_count).val(data.max_player_count);
        } else {
            alert(data['status']);
        }
    });
}

$(document).ready(function () {
    $('#new_game').click(function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        setUserState(userStateCreateGame);

        $('.map_card .map_preview').empty();
        $('#create_game_settings_tab').addClass('disabled');
        $('#create_game_items_tab').addClass('disabled');
        $('#max_player_count').removeAttr('max').val('');
        $('#create_game_container input[name=game_name]').val('');
        $('#map_name')
            .removeAttr('map_id')
            .text('Choose map...');

        api.list_maps({}, function(data) {
            if(data['status'] == 'ok') {
                var list_maps = $('#game_list_maps');
                list_maps.empty();

                _.each(data['maps'], function(gameMap) {
                    var anchor = $('<a>')
                        .attr('href', '#')
                        .text(gameMap.name)
                        .click(_.bind(selectMap, null, gameMap));
                    $('<li>').append(anchor).appendTo(list_maps);
                });

                if(data['maps'] == null) {
                    var anchor = $('<a>').text('No maps found');
                    $('<li>').append(anchor).appendTo(list_maps);
                }
            } else {
                alert(data['status']);
            }
        });
        return false;
    });

    $('#create_game').click(function() {
        var gameName = $('#create_game_container input[name=game_name]').val();
        if (!/^.{2,32}$/.test(gameName)) {
            gameName = $('#map_name').text() + ' (' + $.cookie('bomberman_username') + ')';
        }

        var mapId = parseInt($('#map_name').attr('map_id'));
        api.create_game({
            name: gameName,
            password: $('#create_game_container input[name=game_password]').val(),
            map_id: mapId,
            max_player_count: parseInt($('#max_player_count').val()),
            settings: collectSettings()
        }, function(data) {
            switch(data['status'])
            {
                case 'ok':
                    setUserState(userStateInGame);
                    joinGame(data['game_id'], gameName, mapId);
                    break;
                case 'malformed_request':
                case 'already_in_game':
                case 'map_not_found':
                case 'item_not_specified':
                case 'invalid_item_description':
                    alert(data['status']);
                    break;
            }
        });
        return false;
    });

    $('#join_game').click(function() {
        var joinGameButton = $('#join_game');
        api.join_game({
            game_id: parseInt(joinGameButton.attr('game_id')),
            password: $('#join_game_form input[name=game_password]').val()
        }, function(data) {
            switch(data['status']) {
            case 'ok':
                setUserState(userStateInGame);
                joinGame(
                    joinGameButton.attr('game_id'),
                    joinGameButton.attr('game_name'),
                    joinGameButton.attr('map_id'));
                break;
            case 'already_in_game':
            case 'game_not_found':
            case 'game_already_started':
            case 'wrong_game_password':
            case 'game_is_full':
            case 'malformed_request':
                alert(data['status']);
                break;
            }
        })
        return false;
    });

    $('#start_game').click(function() {
        if ($(this).hasClass('disabled')) {
            return false;
        }
        api.start_game({}, function(data) {
            if(data['status'] != 'ok') {
                alert(data['status']);
            }
            setUserState(userStateWaiting);
        });
        return false;
    });

    $('#leave_game').click(function() {
        api.leave_game({}, function(data) {
            if(data['status'] == 'not_in_game') {
                alert('User not in game');
            }
            gameId = null;
            loadGames();
            setUserState(userStateLoggedIn);
        });
        return false;
    });

    $('#send_message').click(function() {
        api.send_message({
            message: $('#send_message_form input[name=message]').val()
        }, function(data) {
            $('#send_message_form input[name=message]').val('');
        });
        return false;
    });

    var createGameTabNames = ['create_game_basic', 'create_game_settings', 'create_game_items'];
    $('#create_game_basic_tab').click(function() {
        selectTab(createGameTabNames, $(this));
        return false;
    });
    $('#create_game_settings_tab').click(function() {
        selectTab(createGameTabNames, $(this));
        return false;
    });
    $('#create_game_items_tab').click(function() {
        selectTab(createGameTabNames, $(this));
        return false;
    });
});

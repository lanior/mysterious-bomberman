var gamesIntervalID = null;
var gameEnv = null;
var gameCanvas = null;
var gameRenderer = null;
var transport;

var userStateAnonymous = 0,
    userStateLoggedIn = 1,
    userStatePreviewGame = 2,
    userStateCreateGame = 3,
    userStateUploadMap = 4,
    userStateInGame = 5,
    userStateWaiting = 6,
    userStateGameStarted = 7;

var transitions = [
    [true, true, true, true, true, true, true, true],
    [true, true, true, true, true, true, true, true],
    [false, true, true, true, true, false, false, false],
    [false, true, true, true, true, false, false, false],
    [false, true, true, true, true, false, false, false],
    [true, false, true, true, false, true, false, false],
    [true, false, false, false, false, true, true, false],
    [true, false, false, false, false, true, true, true]
];

var userState = userStateAnonymous;

function setUserState(newState) {
    if (!transitions[newState][userState]) {
        return;
    }

    if (newState != userStateAnonymous) {
        $('#username').text($.cookie('bomberman_username'));
    }

    if (newState == userStateLoggedIn) {
        $('#list_players').empty();
        $('#messages').empty();
    }

    if (userState == userStateCreateGame && newState != userState) {
        $('#create_game_settings .settings').empty();
        $('#create_game_items').empty();
    } else if (userState == userStateUploadMap && newState != userState) {
        $('#upload_map_settings .settings').empty();
        $('#upload_map_items').empty();
    }

    var globalLoggedIn = [
        userStateLoggedIn,
        userStatePreviewGame,
        userStateCreateGame,
        userStateUploadMap
    ].indexOf(newState) >= 0;

    $('#login_form').toggle(newState == userStateAnonymous);
    $('.not_anonymous').toggle(newState != userStateAnonymous);
    $('.logged_in').toggle(globalLoggedIn);
    $('#create_game_container').toggle(newState == userStateCreateGame);
    $('#upload_map_container').toggle(newState == userStateUploadMap);
    $('#join_game_form').toggle(newState == userStatePreviewGame);
    $('.in_game').toggle([userStateInGame, userStateWaiting, userStateGameStarted].indexOf(newState) >= 0);
    $('#start_game').toggleClass('disabled', newState != userStateInGame);
    $('#new_game').toggleClass('disabled', newState == userStateCreateGame);
    $('#new_map').toggleClass('disabled', newState == userStateUploadMap);

    if (globalLoggedIn && gamesIntervalID == null) {
        gamesIntervalID = setInterval(loadGames, 5 * 1000);
    } else if (!globalLoggedIn && gamesIntervalID != null) {
        clearInterval(gamesIntervalID);
        gamesIntervalID = null;
    }

    if (gameEnv !== null) {
        gameEnv.destroy();
        gameEnv = null;
    }

    if (newState == userStateGameStarted) {
        gameEnv = new GameEnvironment(transport, gameCanvas, gameRenderer);
    }
    userState = newState;
}

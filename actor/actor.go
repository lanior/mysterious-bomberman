package actor

import (
	"bytes"
	"code.google.com/p/go.net/websocket"
	"encoding/json"
	"fmt"
	"github.com/lanior/mysterious-bomberman/server"
	"github.com/lanior/scheme"
	"io/ioutil"
	"net"
	"net/http"
	"net/url"
	"reflect"
	"regexp"
	"syscall"
	"time"
)

var (
	TypeMap    = reflect.TypeOf(map[string]interface{}{})
	TypeString = reflect.TypeOf("")
	TypeBool   = reflect.TypeOf(true)
	TypeFloat  = reflect.TypeOf(float64(1))
	TypeInt    = TypeFloat
	TypeArray  = reflect.TypeOf([]interface{}{})
)

type Actor struct {
	Sid        string
	WSUrl      string
	WSDeadline time.Duration

	url     string
	mapping map[reflect.Type]reflect.Type
	logger  ActorLogger

	ws *websocket.Conn
}

type ActorLogger interface {
	Request(data []byte, sid string)
	Response(data []byte, sid string)

	WSConnect(url string, sid string)
	WSConnected(sid string)
	WSExpectEvent(event string, sid string)
	WSGotEvent(data []byte, sid string)
	WSGotExpectedEvent(data []byte, sid string)
	WSSendEvent(data []byte, sid string)
}

func New(url string, timeout int, logger ActorLogger) *Actor {
	return &Actor{
		WSDeadline: time.Duration(timeout) * time.Millisecond,
		url:        url,
		mapping:    make(map[reflect.Type]reflect.Type),
		logger:     logger,
	}
}

func panicf(format string, args ...interface{}) {
	panic(fmt.Errorf(format, args...))
}

func catch(fn func()) (err interface{}) {
	defer func() {
		err = recover()
	}()
	fn()
	return nil
}

func (a *Actor) dispatch(data []byte) ([]byte, error) {
	url, err := url.ParseRequestURI(a.url)
	if err != nil {
		return nil, fmt.Errorf("API: cannot parse API url: %s", err)
	}

	q := url.Query()
	q.Set("callback", "c")
	q.Set("q", string(data))
	url.RawQuery = q.Encode()

	resp, err := http.Get(url.String())
	if err != nil {
		return nil, fmt.Errorf("API: network error: %s", err)
	}
	defer resp.Body.Close()

	result, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, fmt.Errorf("API: cannot read response body: %s", err)
	}

	if len(result) > 3 {
		start := bytes.IndexAny(result, "(")
		if start == -1 {
			start = 1
		}
		end := bytes.LastIndexAny(result, ")")
		if end == -1 {
			end = len(result) - 1
		}
		return result[start+1 : end], nil
	}
	return result, nil
}

func (a *Actor) SetResponseType(request, response interface{}) {
	t := reflect.TypeOf(request)
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	a.mapping[t] = reflect.TypeOf(response)
}

func (a *Actor) Clone() *Actor {
	return &Actor{
		Sid:        a.Sid,
		WSUrl:      a.WSUrl,
		WSDeadline: a.WSDeadline,
		url:        a.url,
		mapping:    a.mapping,
		logger:     a.logger,
	}
}

func (a *Actor) RawQuery(query []byte) Q {
	if a.logger != nil {
		a.logger.Request(query, a.Sid)
	}

	resp, err := a.dispatch(query)
	if err != nil {
		panicf("Actor.RawQuery: Actor.dispatch: %s", err)
	}

	if a.logger != nil {
		a.logger.Response(resp, a.Sid)
	}

	var result Q
	if err := json.Unmarshal(resp, &result); err != nil {
		panicf("Actor.RawQuery: json.Unmarshal: %s", err)
	}
	return result
}

func (a *Actor) Query(queryStruct interface{}, status server.ResponseStatus) interface{} {
	query, t := a.PrepareQuery(queryStruct)
	data, err := json.Marshal(query)
	if err != nil {
		panicf("Actor.Query: json.Marshal: %s", err)
	}

	q := a.RawQuery(data)

	anyErr := catch(func() {
		q.AssertStatus(status)
	})
	if anyErr != nil {
		var repr string
		if v, ok := anyErr.(error); ok {
			repr = v.Error()
		} else {
			repr = fmt.Sprintf("%v", err)
		}
		panicf("Actor.Query: %s: %v", query["action"], repr)
	}

	if responseType, ok := a.mapping[t]; ok && status == "ok" {
		v := reflect.New(responseType.Elem())
		q.Unmarshal(v.Interface())
		return v.Interface()
	}
	return q
}

func (a *Actor) PrepareQuery(queryStruct interface{}) (map[string]interface{}, reflect.Type) {
	t := reflect.TypeOf(queryStruct)
	if t.Kind() == reflect.Ptr {
		t = t.Elem()
	}

	if t.Kind() != reflect.Struct {
		panic("Actor.PrepareQuery: expected struct")
	}

	s, err := scheme.Describe(queryStruct)
	if err != nil {
		panicf("Actor.PrepareQuery: scheme.Describe: %s", err)
	}

	query, err := s.Marshal(queryStruct)
	if err != nil {
		panicf("Actor.PrepareQuery: scheme.Marshal: %s", err)
	}

	query["action"] = scheme.ToUnderscores(t.Name())
	if a.Sid != "" {
		query["sid"] = a.Sid
	}
	return query, t
}

func (a *Actor) Ok(query interface{}) interface{} {
	return a.Query(query, server.StatusOk)
}

func (a *Actor) Malformed(query interface{}) interface{} {
	return a.Query(query, server.ErrMalformedRequest)
}

func (a *Actor) Status(status server.ResponseStatus, query interface{}) interface{} {
	return a.Query(query, status)
}

func (a *Actor) wsConnect() {
	if a.ws == nil {
		if a.logger != nil {
			a.logger.WSConnect(a.WSUrl, a.Sid)
		}

		var err error
		a.ws, err = websocket.Dial(a.WSUrl, "", "http://localhost/")
		if err != nil {
			panic(fmt.Errorf("Actor.wsConnect: websocket.Dial: %s", err))
		}

		if a.WSDeadline > 0 {
			a.ws.SetDeadline(time.Now().Add(a.WSDeadline))
		}

		if a.logger != nil {
			a.logger.WSConnected(a.Sid)
		}
	}
}

func actionName(event interface{}) string {
	t := reflect.TypeOf(event)
	for t.Kind() == reflect.Ptr {
		t = t.Elem()
	}
	return scheme.ToUnderscores(t.Name())
}

func eventName(event interface{}) string {
	return actionName(event)[2:]
}

func (a *Actor) SendEvent(event interface{}) {
	a.wsConnect()
	s, err := scheme.Describe(reflect.TypeOf(event))
	if err != nil {
		panicf("Actor.SendEvent: scheme.Describe: %s", err)
	}

	result, err := s.Marshal(event)
	if err != nil {
		panicf("Actor.SendEvent: scheme.Marshal: %s", err)
	}

	if _, ok := result["event"]; !ok {
		result["event"] = eventName(event)
	}

	if a.logger != nil {
		data, err := json.Marshal(result)
		if err == nil {
			a.logger.WSSendEvent(data, a.Sid)
		}
	}

	if err := websocket.JSON.Send(a.ws, result); err != nil {
		panicf("Actor.SendEvent: websocket.JSON.Send: %s", err)
	}
}

func (a *Actor) ExpectEvent(eventPtr interface{}) {
	a.wsConnect()

	expectedEvent := eventName(eventPtr)
	if a.logger != nil {
		a.logger.WSExpectEvent(expectedEvent, a.Sid)
	}

	s, err := scheme.Describe(eventPtr)
	if err != nil {
		panicf("Actor.ExpectEvent %s: scheme.Describe: %s", expectedEvent, err)
	}

	for {
		var data map[string]interface{}

		for {
			if err := websocket.JSON.Receive(a.ws, &data); err != nil {
				if err2, ok := err.(*net.OpError); ok {
					if err2.Err == syscall.EAGAIN {
						continue
					}
				}
				panicf("Actor.ExpectEvent %s: websocket.JSON.Receive: %s", expectedEvent, err)
			}
			break
		}

		if data["event"] == expectedEvent {
			if a.logger != nil {
				d, err := json.Marshal(data)
				if err == nil {
					a.logger.WSGotExpectedEvent(d, a.Sid)
				}
			}

			if err := s.Unmarshal(data, eventPtr); err != nil {
				panicf("Actor.ExpectEvent %s: scheme.Unmarshal: %s", expectedEvent, err)
			}
			break
		} else if a.logger != nil {
			d, err := json.Marshal(data)
			if err == nil {
				a.logger.WSGotEvent(d, a.Sid)
			}
		}
	}
}

func (a *Actor) WSAuth() {
	a.SendEvent(server.WSAuth{Sid: a.Sid})
	a.ExpectEvent(&server.WSAuthSuccess{})
}

type Q map[string]interface{}

func (q Q) GetMap(field string) Q {
	q.AssertFieldType(field, TypeMap)
	return Q(q[field].(map[string]interface{}))
}

func (q Q) GetString(field string) string {
	q.AssertFieldType(field, TypeString)
	return q[field].(string)
}

func (q Q) GetArray(field string) []interface{} {
	q.AssertFieldType(field, TypeArray)
	return q[field].([]interface{})
}

func (q Q) GetInt(field string) int {
	q.AssertFieldType(field, TypeInt)
	return int(q[field].(float64))
}

func (q Q) Unmarshal(target interface{}) {
	action := actionName(target)
	s, err := scheme.DescribeUsing(target, scheme.Underscores)
	if err != nil {
		panicf("%s: Scheme.Describe: %s", action, err)
	}
	if err := s.Unmarshal(map[string]interface{}(q), target); err != nil {
		panicf("%s: scheme.Unmarshal: %s", action, err)
	}
}

func (q Q) AssertFieldType(field string, fieldType reflect.Type) Q {
	q.AssertHasField(field)
	if realType := reflect.TypeOf(q[field]); realType != fieldType {
		panicf("field %s is of type %s, %s expected", field, realType, fieldType)
	}
	return q
}

func (q Q) AssertHasField(field string) Q {
	_, ok := q[field]
	if !ok {
		panicf("missing field `%s`", field)
	}
	return q
}

func (q Q) AssertFieldEqual(field string, value interface{}) Q {
	q.AssertHasField(field)
	v := q[field]
	if !reflect.DeepEqual(v, value) {
		panicf("expected %s `%s`, got `%s`", field, value, v)
	}
	return q
}

func (q Q) AssertMatchRegexp(field string, regexp *regexp.Regexp) Q {
	value := q.GetString(field)
	if ok := regexp.MatchString(value); !ok {
		panicf("value `%s` doesn't match regexp `%s`", value, regexp.String())
	}
	return q
}

func (q Q) AssertStatus(status server.ResponseStatus) Q {
	return q.AssertFieldEqual("status", string(status))
}

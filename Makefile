.PHONY: test run_server build_server build_tester

test: build_tester
	@killall -q -9 tester || true
	./tester/tester -embedded

run_server: build_server
	@killall -q -9 mysterious-bomberman || true
	./mysterious-bomberman -debug=true

build_server:
	go build

build_tester:
	cd tester && go build

run_engine_tests:
	cd engine && go test

#!/usr/bin/env python
import os

ROOT_DIR = os.environ.get('ROOT_DIR', os.path.abspath(os.path.dirname(__file__)))
RELEASE_DIR = os.environ.get('RELEASE_DIR', os.path.join(ROOT_DIR, 'release'))

projects = {
    'server': ROOT_DIR,
    'tester': os.path.join(ROOT_DIR, 'tester'),
}

targets = [
    'darwin/amd64',
    'linux/386',
    'linux/amd64',
    'windows/386',
    'windows/amd64',
]

def release():
    try:
        os.makedirs(RELEASE_DIR)
    except:
        pass

    for project, project_dir in projects.items():
        for target in targets:
            osname, arch = target.split('/')
            ext = '.exe' if osname == 'windows' else ''
            cmd = 'cd {0} && CGO_ENABLED=0 GOOS={os} GOARCH={arch} go build -o {1}/{project}-{os}-{arch}{ext}'.format(
                project_dir,
                RELEASE_DIR,
                project=project,
                ext=ext,
                arch=arch,
                os=osname,
            )
            print(cmd)
            os.system(cmd)


if __name__ == '__main__':
    release()

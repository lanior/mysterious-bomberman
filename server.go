package main

import (
	"flag"
	"fmt"
	"github.com/lanior/mysterious-bomberman/server"
	"labix.org/v2/mgo"
	"log"
	"net/http"
	"os"
	"time"
)

var (
	httpAddr     = flag.String("http", "0.0.0.0:8080", "HTTP service address")
	publicDomain = flag.String("domain", "auto", "Public domain name")
	debugMode    = flag.Bool("debug", false, "Enables debug mode")
	serveClient  = flag.Bool("serve", true, "Serve client files")
	showHelp     = flag.Bool("help", false, "Show this message")
	dbHost       = flag.String("dbhost", "localhost:27017", "MongoDB hostname")
	dbName       = flag.String("dbname", "BombStorage", "MongoDB database name")
)

func main() {
	flag.Parse()

	if *showHelp || flag.Arg(0) != "" {
		fmt.Printf("usage: %s\n", os.Args[0])
		flag.PrintDefaults()
		return
	}

	webSocketUrl := "auto"
	if *publicDomain != "auto" {
		domain := *publicDomain
		if domain == "" {
			domain = *httpAddr
		}
		webSocketUrl = "ws://" + domain + "/ws/api/"
	}

	server, err := server.New(&server.Config{
		DebugMode:    *debugMode,
		WebSocketUrl: webSocketUrl,
		DBConfig: &mgo.DialInfo{
			Addrs:    []string{*dbHost},
			Database: *dbName,
			Timeout:  5 * time.Second,
		},
		Logger: log.New(os.Stderr, "", log.Ldate|log.Ltime),
	})

	if err != nil {
		log.Fatalf("server: %s", err)
	}

	debugModeMsg := ""
	if *debugMode {
		debugModeMsg = " (debug mode)"
	} else {
		server.LoadMaps("maps/")
	}

	log.Printf("Listening %s%s", *httpAddr, debugModeMsg)

	if *serveClient {
		http.Handle("/", http.RedirectHandler("/client/", http.StatusFound))
		http.Handle("/client/", http.FileServer(http.Dir("")))
	}
	http.Handle("/api/", server.Handler())
	log.Fatalf("http: %s", http.ListenAndServe(*httpAddr, nil))
}

package main

import (
	"github.com/lanior/mysterious-bomberman/actor"
	. "github.com/lanior/mysterious-bomberman/server"
	. "github.com/lanior/mysterious-bomberman/tester/testlib"
)

func MapSpec(c Context, api *actor.Actor) {
	api.Ok(Reset{})

	user := createUserAndLogin(api, "pepper", "potts")
	c.Test("Upload map", func(c Context) {
		c.Test("empty name", func(c Context) {
			user.Malformed(UploadMap{Name: "", Field: basicField, Settings: gameSettings})
		})

		c.Test("bad field", func(c Context) {
			user.Malformed(UploadMap{Name: "", Field: []string{"..", "..", ".."}, Settings: gameSettings})
		})

		c.Test("bad settings", func(c Context) {
			//settings except items
			c.Test("item support", func(c Context) {
				settings := gameSettings
				settings.Items = map[string]ItemDescription{}
				for key, item := range basicItems {
					settings.Items[key] = item
					if len(settings.Items) < len(basicItems) {
						user.Malformed(UploadMap{Name: "V", Field: basicField, Settings: settings})
					}
				}
			})

			c.Test("basic item descriptions", func(c Context) {
				settings := gameSettings
				settings.Items = map[string]ItemDescription{}
				for key, item := range basicItems {
					settings.Items[key] = item
				}
				settings.Items["kicker"] = ItemDescription{StartCount: -1, MaxCount: 0, Weight: 0}
				user.Malformed(UploadMap{Name: "new_map", Field: basicField, Settings: settings})
				settings.Items["kicker"] = ItemDescription{StartCount: 0, MaxCount: -1, Weight: 0}
				user.Malformed(UploadMap{Name: "new_map", Field: basicField, Settings: settings})
				settings.Items["kicker"] = ItemDescription{StartCount: 0, MaxCount: 0, Weight: -1}
				user.Malformed(UploadMap{Name: "new_map", Field: basicField, Settings: settings})
				settings.Items["kicker"] = ItemDescription{StartCount: 2, MaxCount: 1, Weight: 1}
				user.Malformed(UploadMap{Name: "new_map", Field: basicField, Settings: settings})
			})
		})

		validQuery := UploadMap{Name: "good_map", Field: basicField, Settings: gameSettings}
		c.Test("correct map", func(c Context) {
			user.Ok(validQuery)
		})

		c.Test("malformed input", func(c Context) {
			marQuery(api, validQuery)
		})
	})

	c.Test("Get map", func(c Context) {
		c.Test("non existent map", func(c Context) {
			user.Status(ErrMapNotFound, GetMap{MapId: 1})
		})

		mapId := createMap(user)
		c.Test("anonymous cannot get map", func(c Context) {
			api.Malformed(GetMap{MapId: mapId})
		})

		c.Test("user can get map", func(c Context) {
			r := user.Ok(GetMap{MapId: mapId}).(*GetMapResponse)
			assertNilError(ValidateGameSettings(&r.Settings))
		})

		c.Test("malformed input", func(c Context) {
			marQuery(user, GetMap{MapId: mapId})
		})
	})

	c.Test("List maps", func(c Context) {
		c.Test("anonymous cannot get map list", func(c Context) {
			api.Malformed(ListMaps{})
		})

		c.Test("user can get map list", func(c Context) {
			mapId := createMap(user)
			mapId2 := createMap(user)
			mapList := user.Ok(ListMaps{}).(*ListMapsResponse).Maps
			assert(len(mapList) == 2, "wrong list size")
			for _, briefMap := range mapList {
				assert(briefMap.MapId == mapId || briefMap.MapId == mapId2, "unexpected map in list")
			}
			assert(mapList[0].MapId != mapList[1].MapId, "map duplicated")
		})
	})
}

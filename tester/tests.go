package main

import (
	"encoding/json"
	"fmt"
	"github.com/lanior/mysterious-bomberman/actor"
	. "github.com/lanior/mysterious-bomberman/server"
	. "github.com/lanior/mysterious-bomberman/tester/testlib"
	"net/url"
	"strings"
)

const cellSize = 10000

type RequiredExtensionsResponse struct {
	Extensions struct {
		Debug interface{} `field:"debug.v0"`
		Core  struct {
			WebSocketUrl        string `field:"websocket_url"`
			MinMapSize          int
			MaxMapSize          int
			DefaultGameSettings GameSettings
		} `field:"core.v0"`
	}
}

func login(api *actor.Actor, username string, password string) *actor.Actor {
	q := api.Ok(Login{Username: username, Password: password})
	actor := api.Clone()
	actor.Sid = q.(*LoginResponse).Sid
	return actor
}

func createUserAndLogin(api *actor.Actor, username string, password string) *actor.Actor {
	api.Ok(Register{Username: username, Password: password})
	return login(api, username, password)
}

func createGame(user *actor.Actor, password string) int {
	mapId := createMap(user)
	q := user.Ok(CreateGame{Name: "good_game", Password: password, MapId: mapId, MaxPlayerCount: 4, Settings: gameSettings})
	return q.(*CreateGameResponse).GameId
}

func createMap(user *actor.Actor) int {
	q := user.Ok(UploadMap{Name: "good_map", Field: basicField, Settings: gameSettings})
	return q.(*UploadMapResponse).MapId
}

func assert(expr bool, message string, args ...interface{}) {
	if !expr {
		panic(fmt.Errorf(message, args...))
	}
}

func assertNilError(err error) {
	if err != nil {
		panic(err)
	}
}

func getRunner(api *actor.Actor) *TestRunner {
	api.SetResponseType(&Extensions{}, &RequiredExtensionsResponse{})
	api.SetResponseType(&Login{}, &LoginResponse{})
	api.SetResponseType(&PlayerState{}, &PlayerStateResponse{})
	api.SetResponseType(&CreateGame{}, &CreateGameResponse{})
	api.SetResponseType(&JoinGame{}, &JoinGameResponse{})
	api.SetResponseType(&UploadMap{}, &UploadMapResponse{})
	api.SetResponseType(&ListGames{}, &ListGamesResponse{})
	api.SetResponseType(&ListMaps{}, &ListMapsResponse{})
	api.SetResponseType(&GameSetTick{}, &GameSetTickResponse{})
	api.SetResponseType(&GetMap{}, &GetMapResponse{})

	withApi := func(fn func(c Context, api *actor.Actor)) TestFunc {
		return func(c Context) {
			fn(c, api)
		}
	}

	runner := NewTestRunner()
	runner.AddSection("Core", func(c Context) {
		ok := c.Test("core.v0 and debug.v0 extensions are supported", func(c Context) {
			response := api.Ok(Extensions{}).(*RequiredExtensionsResponse)
			assertNilError(ValidateGameSettings(&response.Extensions.Core.DefaultGameSettings))
			api.WSUrl = response.Extensions.Core.WebSocketUrl

			wsUrl, err := url.Parse(api.WSUrl)
			assert(err == nil, "websocket_url: parse: %s", err)
			assert(wsUrl.Scheme == "ws" || wsUrl.Scheme == "wss", "websocket_url: unknown scheme %s, expected ws or wss", wsUrl.Scheme)
		})

		c.TestIf(ok, "reset is supported", func(c Context) {
			api.Ok(Reset{})
		})
	})
	runner.AddSection("User", withApi(UserSpec))
	runner.AddSection("Map", withApi(MapSpec))
	runner.AddSection("Game List", withApi(GameListSpec))
	runner.AddSection("WebSocket", withApi(WebSocketSpec))
	runner.AddSection("Engine", withApi(EngineSpec))
	return runner
}

func marQuery(api *actor.Actor, queryStruct interface{}) {
	var query map[string]interface{}
	var ok bool

	if query, ok = queryStruct.(map[string]interface{}); !ok {
		query, _ = api.PrepareQuery(queryStruct)
	}

	type ModifierFunc func(obj map[string]interface{}, key string)

	var test func(obj map[string]interface{}, path []string, msg string, fn ModifierFunc)
	test = func(obj map[string]interface{}, path []string, msg string, fn ModifierFunc) {
		testQuery := func(key string) {
			defer func() {
				if err := recover(); err != nil {
					if e, ok := err.(error); ok {
						pathString := strings.Join(append(path, key), "/")
						panic(fmt.Errorf("marQuery: test with missing field `%s`: %s", pathString, e.Error()))
					}
					panic(err)
				}
			}()

			data, err := json.Marshal(query)
			assert(err == nil, "marQuery: json.Marshal: %s", err)

			q := api.RawQuery(data)

			status := q.GetString("status")
			assert(status == "malformed_request", "expected status `malformed_request`, got `%s`", status)

		}

		var keys []string
		for key := range obj {
			keys = append(keys, key)
		}

		for _, key := range keys {
			oldValue := obj[key]
			fn(obj, key)
			if v, ok := obj[key]; ok && v == oldValue {
				continue
			}

			testQuery(key)
			obj[key] = oldValue

			switch v := obj[key].(type) {
			case map[string]interface{}:
				test(v, append(path, key), msg, fn)
			}
		}
	}

	test(query, nil, "missing field", func(obj map[string]interface{}, key string) {
		delete(obj, key)
	})

	test(query, nil, "string value", func(obj map[string]interface{}, key string) {
		obj[key] = fmt.Sprintf("%s", obj[key])
	})

	test(query, nil, "null value", func(obj map[string]interface{}, key string) {
		obj[key] = nil
	})
}

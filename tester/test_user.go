package main

import (
	"github.com/lanior/mysterious-bomberman/actor"
	. "github.com/lanior/mysterious-bomberman/server"
	. "github.com/lanior/mysterious-bomberman/tester/testlib"
)

func UserSpec(c Context, api *actor.Actor) {
	api.Ok(Reset{})

	c.Test("Registration", func(c Context) {
		c.Test("valid username and password", func(c Context) {
			api.Ok(Register{Username: "foo123", Password: "bar"})
			api.Ok(Register{Username: "_foo_", Password: "bar"})
		})

		c.Test("cannot take same username", func(c Context) {
			api.Ok(Register{Username: "_foo_", Password: "bar"})
			api.Status(ErrUserAlreadyExists, Register{Username: "_foo_", Password: "bar"})
		})

		c.Test("malformed input", func(c Context) {
			api.Malformed(Register{Username: "fo", Password: "bar"})
			api.Malformed(Register{Username: "too_long_username", Password: "bar"})
			api.Malformed(Register{Username: "empty_password", Password: ""})
			marQuery(api, Register{Username: "foo123", Password: "bar"})
		})
	})

	c.Test("Log in", func(c Context) {
		api.Ok(Register{Username: "myata", Password: "salvia"})

		validQuery := Login{Username: "myata", Password: "salvia"}
		c.Test("log in with valid credentials", func(c Context) {
			api.Ok(validQuery)
		})

		c.Test("log in twice", func(c Context) {
			api.Ok(Login{Username: "myata", Password: "salvia"})
			api.Ok(Login{Username: "myata", Password: "salvia"})
		})

		c.Test("user cannot log in with invalid credentials", func(c Context) {
			api.Status(ErrInvalidCredentials, Login{Username: "mya", Password: "salvia"})
			api.Status(ErrInvalidCredentials, Login{Username: "myata", Password: "abcde"})
		})

		c.Test("malformed input", func(c Context) {
			api.Malformed(Login{Username: "fo", Password: "bar"})
			api.Malformed(Login{Username: "too_long_username", Password: "bar"})
			api.Malformed(Login{Username: "empty_password", Password: ""})
			marQuery(api, validQuery)
		})
	})

	user := createUserAndLogin(api, "foo", "bar")
	c.Test("Log out", func(c Context) {
		c.Test("user can log out", func(c Context) {
			user.Ok(Logout{})
		})

		c.Test("user cannot log out twice", func(c Context) {
			user.Ok(Logout{})
			user.Status(ErrInvalidSid, Logout{})
		})
	})

	c.Test("Player state", func(c Context) {
		c.Test("user required", func(c Context) {
			api.Malformed(PlayerState{})
		})

		c.Test("user not in game", func(c Context) {
			state := user.Ok(PlayerState{})
			assert(state.(*PlayerStateResponse).State == "not_in_game", "invalid player state")
		})

		c.Test("user in game", func(c Context) {
			gameId := createGame(user, "")
			state := user.Ok(PlayerState{})
			assert(state.(*PlayerStateResponse).State == "in_game", "invalid player state")
			assert(state.(*PlayerStateResponse).GameId == gameId, "invalid game id")
		})

		c.Test("user in started game", func(c Context) {
			gameId := createGame(user, "")
			user2 := createUserAndLogin(api, "foo2", "bar")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user2.Ok(StartGame{})
			user.Ok(StartGame{})
			state := user.Ok(PlayerState{})
			assert(state.(*PlayerStateResponse).State == "in_started_game", "invalid player state")
			assert(state.(*PlayerStateResponse).GameId == gameId, "invalid game id")
		})
	})
}

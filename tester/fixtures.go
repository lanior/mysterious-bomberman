package main

import (
	. "github.com/lanior/mysterious-bomberman/server"
)

var basicField = []string{
	"########",
	"0.2.1.3.",
	"########",
}

var item = ItemDescription{
	Weight:     2,
	StartCount: 0,
	MaxCount:   5,
}

var basicItems = map[string]ItemDescription{
	"autobomb": ItemDescription{
		Duration:   300,
		MaxCount:   0,
		StartCount: 0,
		Weight:     4,
	},
	"bomb": ItemDescription{
		BombBonus:  1,
		MaxCount:   9,
		StartCount: 1,
		Weight:     18,
	},
	"flame": ItemDescription{
		FlameBonus: 1,
		MaxCount:   12,
		StartCount: 1,
		Weight:     18,
	},
	"goldenflame": ItemDescription{
		FlameBonus: 100,
		MaxCount:   1,
		StartCount: 0,
		Weight:     2,
	},
	"invert": ItemDescription{
		Duration:   300,
		MaxCount:   0,
		StartCount: 0,
		Weight:     4,
	},
	"kicker": ItemDescription{
		MaxCount:   1,
		StartCount: 0,
		Weight:     0,
	},
	"lightspeed": ItemDescription{
		Duration:   300,
		MaxCount:   0,
		Speed:      4995,
		StartCount: 0,
		Weight:     4,
	},
	"nobomb": ItemDescription{
		Duration:   300,
		MaxCount:   0,
		StartCount: 0,
		Weight:     4,
	},
	"speed": ItemDescription{
		MaxCount:   5,
		SpeedBonus: 333,
		StartCount: 0,
		Weight:     18,
	},
	"throw": ItemDescription{
		MaxCount:   1,
		StartCount: 0,
		Weight:     0,
	},
	"turtle": ItemDescription{
		Duration:   300,
		MaxCount:   0,
		Speed:      333,
		StartCount: 0,
		Weight:     4,
	},
}

var gameSettings = GameSettings{
	BlastWaveDuration:      15,
	BombChainReactionDelay: 3,
	BombDelay:              75,
	BombSpeed:              1332,
	CellDecayDuration:      15,
	ItemDropProbability:    55,
	MaxSlideLength:         5000,
	PlayerBaseSpeed:        749,
	Items:                  basicItems,
}

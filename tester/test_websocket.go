package main

import (
	"github.com/lanior/mysterious-bomberman/actor"
	. "github.com/lanior/mysterious-bomberman/server"
	. "github.com/lanior/mysterious-bomberman/tester/testlib"
	"strings"
)

func WebSocketSpec(c Context, api *actor.Actor) {
	api.Ok(Reset{})
	user := createUserAndLogin(api, "foo", "bar")

	c.Test("Auth", func(c Context) {
		c.Test("with valid sid", func(c Context) {
			user.SendEvent(WSAuth{Sid: user.Sid})
			user.ExpectEvent(&WSAuthSuccess{})
		})

		c.Test("with malformed sid", func(c Context) {
			user.SendEvent(WSAuth{Sid: "abc"})
			user.ExpectEvent(&WSProtocolViolation{})
		})

		c.Test("with invalid sid", func(c Context) {
			user.SendEvent(WSAuth{Sid: strings.Repeat("a", 32)})
			user.ExpectEvent(&WSAuthError{})
		})
	})

	user2 := createUserAndLogin(api, "foo2", "bar")
	user2.WSAuth()
	user.WSAuth()

	c.Test("New Message", func(c Context) {
		c.Test("user receives his own message", func(c Context) {
			createGame(user, "")
			user.Ok(SendMessage{Message: "hello world!"})
			var msg WSNewMessage
			user.ExpectEvent(&msg)
			assert(msg.Text == "hello world!" && msg.Username == "foo", "wrong message")
		})

		c.Test("other users receive messages", func(c Context) {
			gameId := createGame(user, "")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user2.Ok(SendMessage{Message: "I'm 2"})
			var msg WSNewMessage
			user.ExpectEvent(&msg)
			user2.ExpectEvent(&WSNewMessage{})
			assert(msg.Text == "I'm 2" && msg.Username == "foo2", "wrong message received by user")
			user.Ok(SendMessage{Message: "I'm 1"})
			user.ExpectEvent(&WSNewMessage{})
			user2.ExpectEvent(&msg)
			assert(msg.Text == "I'm 1" && msg.Username == "foo", "wrong message received by user2")
		})
	})

	c.Test("Player joined game", func(c Context) {
		var pjg WSPlayerJoinedGame
		c.Test("user receives his own action report", func(c Context) {
			createGame(user, "")
			user.ExpectEvent(&pjg)
			assert(pjg.Username == "foo", "wrong username")
		})

		gameId := createGame(user, "")
		user.ExpectEvent(&pjg)
		user2.Ok(JoinGame{GameId: gameId, Password: ""})
		c.Test("new player receives previous action reports", func(c Context) {
			user2.ExpectEvent(&pjg)
			assert(pjg.Username == "foo", "wrong username")
			user2.ExpectEvent(&pjg)
			assert(pjg.Username == "foo2", "wrong username")
		})

		c.Test("other players receive action report", func(c Context) {
			user.ExpectEvent(&pjg)
			assert(pjg.Username == "foo2", "wrong username")
		})
	})
}

package main

import (
	"github.com/lanior/mysterious-bomberman/actor"
	. "github.com/lanior/mysterious-bomberman/server"
	. "github.com/lanior/mysterious-bomberman/tester/testlib"
)

func GameListSpec(c Context, api *actor.Actor) {
	api.Ok(Reset{})

	user := createUserAndLogin(api, "foo", "bar")
	mapId := createMap(user)

	c.Test("Create Game", func(c Context) {
		validQuery := CreateGame{Name: "good_game", Password: "", MapId: mapId, MaxPlayerCount: 4, Settings: gameSettings}
		c.Test("game without password", func(c Context) {
			user.Ok(validQuery)
		})

		c.Test("game with password", func(c Context) {
			user.Ok(CreateGame{Name: "good_game", Password: "111", MapId: mapId, MaxPlayerCount: 4, Settings: gameSettings})
		})

		c.Test("cannot create game with invalid map", func(c Context) {
			user.Status(ErrMapNotFound, CreateGame{Name: "mapless_game", Password: "", MapId: -1, MaxPlayerCount: 4, Settings: gameSettings})
		})

		c.Test("anonymous cannot create a game", func(c Context) {
			api.Malformed(CreateGame{Name: "anonymous_game", Password: "", MapId: mapId, MaxPlayerCount: 4, Settings: gameSettings})
		})

		c.Test("malformed input", func(c Context) {
			user.Malformed(CreateGame{Name: "e", Password: "", MapId: mapId, MaxPlayerCount: 4, Settings: gameSettings})
			marQuery(user, validQuery)
		})
	})

	user2 := createUserAndLogin(api, "foo2", "bar")
	c.Test("Join Game", func(c Context) {
		c.Test("game must exist", func(c Context) {
			user2.Status(ErrGameNotFound, JoinGame{GameId: -1, Password: ""})
			user2.Status(ErrGameNotFound, JoinGame{GameId: -1, Password: "555"})
		})

		c.Test("user can join a game with empty password", func(c Context) {
			gameId := createGame(user, "")
			user2.Status(ErrWrongGamePassword, JoinGame{GameId: gameId, Password: "qwe"})
			r := user2.Ok(JoinGame{GameId: gameId, Password: ""}).(*JoinGameResponse)
			assertNilError(ValidateGameSettings(&r.Settings))
		})

		c.Test("user can join a game with non-empty password", func(c Context) {
			gameId := createGame(user, "555")
			user2.Status(ErrWrongGamePassword, JoinGame{GameId: gameId, Password: ""})
			r := user2.Ok(JoinGame{GameId: gameId, Password: "555"}).(*JoinGameResponse)
			assertNilError(ValidateGameSettings(&r.Settings))
		})

		c.Test("user can be only in one game", func(c Context) {
			gameId := createGame(user, "555")
			createGame(user2, "555")
			user2.Status(ErrAlreadyInGame, JoinGame{GameId: gameId, Password: "555"})
		})

		c.Test("user cannot join full game", func(c Context) {
			gameId := createGame(user, "")
			user3 := createUserAndLogin(api, "foo3", "bar")
			user4 := createUserAndLogin(api, "foo4", "bar")
			user5 := createUserAndLogin(api, "foo5", "bar")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user3.Ok(JoinGame{GameId: gameId, Password: ""})
			user4.Ok(JoinGame{GameId: gameId, Password: ""})
			user5.Status(ErrGameIsFull, JoinGame{GameId: gameId, Password: ""})
		})

		c.Test("malformed input", func(c Context) {
			gameId := createGame(user, "")
			marQuery(user, JoinGame{GameId: gameId, Password: ""})
		})
	})

	c.Test("Leave Game", func(c Context) {
		c.Test("user can leave game", func(c Context) {
			createGame(user, "")
			user.Ok(LeaveGame{})
			user.Status(ErrNotInGame, LeaveGame{})
		})

		c.Test("game without players must be closed", func(c Context) {
			gameId := createGame(user, "")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})

			user.Ok(LeaveGame{})
			user2.Ok(LeaveGame{})

			user2.Status(ErrGameNotFound, JoinGame{GameId: gameId, Password: ""})
		})

		c.Test("leave game on logout", func(c Context) {
			gameId := createGame(user, "")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user2.Ok(Logout{})
			user2 = login(api, "foo2", "bar")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
		})

		c.Test("leaver can trigger game start", func(c Context) {
			user3 := createUserAndLogin(api, "foo3", "bar")

			gameId := createGame(user, "")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user3.Ok(JoinGame{GameId: gameId, Password: ""})

			user.WSAuth()

			user.Ok(StartGame{})
			user2.Ok(StartGame{})

			user3.Ok(LeaveGame{})
			user3.Status(ErrGameAlreadyStarted, JoinGame{GameId: gameId, Password: ""})

			playerState := user.Ok(PlayerState{}).(*PlayerStateResponse)
			assert(playerState.State == "in_started_game", "game is still not running")

			playerState2 := user2.Ok(PlayerState{}).(*PlayerStateResponse)
			assert(playerState2.State == "in_started_game", "game is still not running")

			user.ExpectEvent(&WSGameStarted{})
		})
	})

	user3 := createUserAndLogin(api, "foo3", "bar")
	c.Test("Start Game", func(c Context) {
		c.Test("user can start game", func(c Context) {
			createGame(user, "")
			user.Ok(StartGame{})
		})

		c.Test("user cannot join game if game started", func(c Context) {
			gameId := createGame(user, "")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user2.Ok(StartGame{})
			user.Ok(StartGame{})
			user3.Status(ErrGameAlreadyStarted, JoinGame{GameId: gameId, Password: ""})
		})

		c.Test("game doesn't start with only one player", func(c Context) {
			gameId := createGame(user, "")
			user.Ok(StartGame{})
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
		})

		c.Test("user doesn't count as ready-to-play when leaves", func(c Context) {
			gameId := createGame(user, "")
			user.Ok(StartGame{})
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user.Ok(LeaveGame{})
			user2.Ok(StartGame{})
			user.Ok(JoinGame{GameId: gameId, Password: ""})
		})

		c.Test("game goes on if player leaves after start", func(c Context) {
			gameId := createGame(user, "")
			user2.Ok(JoinGame{GameId: gameId, Password: ""})
			user3.Ok(JoinGame{GameId: gameId, Password: ""})
			user.Ok(StartGame{})
			user2.Ok(StartGame{})
			user3.Ok(StartGame{})
			user.Ok(LeaveGame{})
			user.Status(ErrGameAlreadyStarted, JoinGame{GameId: gameId, Password: ""})
		})
	})

	c.Test("Send Message", func(c Context) {
		c.Test("user cannot send message if not in game", func(c Context) {
			user.Status(ErrNotInGame, SendMessage{Message: "1"})
		})

		validQuery := SendMessage{Message: "hello world!"}
		c.Test("user can send message", func(c Context) {
			createGame(user, "")
			user.Ok(validQuery)
		})

		c.Test("user cannot send empty message", func(c Context) {
			user.Status(ErrMalformedRequest, SendMessage{Message: ""})
		})

		c.Test("malformed input", func(c Context) {
			createGame(user, "")
			marQuery(user, validQuery)
		})
	})

	c.Test("List Games", func(c Context) {
		c.Test("anonymous cannot get game list", func(c Context) {
			api.Malformed(ListGames{})
		})

		gameId := createGame(user, "")
		c.Test("total_slots is correct", func(c Context) {
			gameList := user3.Ok(ListGames{}).(*ListGamesResponse).Games
			assert(len(gameList) == 1, "wrong list size")
			assert(gameList[0].TotalSlots == 4, "map contains 4 slots, game offers %d slots", gameList[0].TotalSlots)
		})

		gameId2 := createGame(user2, "password")
		c.Test("list is correct", func(c Context) {
			gameList := user3.Ok(ListGames{}).(*ListGamesResponse).Games
			assert(len(gameList) == 2, "wrong list size")
			for _, game := range gameList {
				assert(game.Id == gameId || game.Id == gameId2, "unexpected game in list")
			}
			assert(gameList[0].Id != gameList[1].Id, "game duplicated")
		})

		c.Test("has_password is calculated correctly", func(c Context) {
			gameList := user3.Ok(ListGames{}).(*ListGamesResponse).Games
			assert(len(gameList) == 2, "wrong list size")
			if gameList[0].Id == gameId {
				assert(!gameList[0].HasPassword, "game without password marked as game with password")
				assert(gameList[1].HasPassword, "game with password marked as game without password")
			} else {
				assert(!gameList[1].HasPassword, "game without password marked as game with password")
				assert(gameList[0].HasPassword, "game with password marked as game without password")
			}
		})

		c.Test("full game is hidden", func(c Context) {
			user4 := createUserAndLogin(api, "foo4", "bar")
			user5 := createUserAndLogin(api, "foo5", "bar")
			user3.Ok(JoinGame{GameId: gameId, Password: ""})
			user4.Ok(JoinGame{GameId: gameId, Password: ""})
			user5.Ok(JoinGame{GameId: gameId, Password: ""})
			gameList := user2.Ok(ListGames{}).(*ListGamesResponse).Games
			assert(len(gameList) == 1, "wrong list size")
			assert(gameList[0].Id == gameId2, "wrong game in list")
		})

		c.Test("started game is hidden", func(c Context) {
			user3.Ok(JoinGame{GameId: gameId, Password: ""})
			user3.Ok(StartGame{})
			user.Ok(StartGame{})
			gameList := user2.Ok(ListGames{}).(*ListGamesResponse).Games
			assert(len(gameList) == 1, "wrong list size")
			assert(gameList[0].Id == gameId2, "wrong game in list")
		})
	})
}

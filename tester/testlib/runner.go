package testlib

type testSection struct {
	Name string
	Func TestFunc
}

type TestRunner struct {
	sections []*testSection
}

type Reporter interface {
	BeforeTesting()
	AfterTesting()

	TestStart(name string)
	TestOk(name string)
	TestSkipped(name string)
	TestFailed(name string, err interface{})
}

type testUnwinder struct {
	HasMore bool
	Success bool
}

func NewTestRunner() *TestRunner {
	return &TestRunner{}
}

type TestFunc func(Context)

func (tr *TestRunner) AddSection(name string, fn TestFunc) {
	tr.sections = append(tr.sections, &testSection{
		Name: name,
		Func: fn,
	})
}

func (tr *TestRunner) Run(reporter Reporter, untilFirstFail bool) bool {
	reporter.BeforeTesting()

	totalSuccess := true
	c := newTestContext("", reporter, nil)
	for _, section := range tr.sections {
		succeeded := false
		hasMore := true
		for hasMore {
			succeeded, hasMore = tr.runSection(totalSuccess, c, section)
			if !succeeded && untilFirstFail {
				reporter.AfterTesting()
				return false
			}
		}

		if !succeeded {
			totalSuccess = false
		}
	}
	reporter.AfterTesting()
	return totalSuccess
}

func (tr *TestRunner) runSection(cond bool, c Context, section *testSection) (result bool, hasMore bool) {
	defer func() {
		if err := recover(); err != nil {
			report := err.(*testUnwinder)
			hasMore = report.HasMore
			result = report.Success
		}
	}()
	return c.TestIf(cond, section.Name, section.Func), false
}

type Context interface {
	Name() string
	Test(name string, fn TestFunc) bool
	TestIf(cond bool, name string, fn TestFunc) bool
}

type testResult struct {
	Success bool
	HasMore bool
	Context *testContext
}

type testContext struct {
	name       string
	incomplete bool
	reporter   Reporter
	parent     *testContext

	executedTests map[string]*testResult
}

func newTestContext(name string, reporter Reporter, parent *testContext) *testContext {
	return &testContext{
		name:          name,
		reporter:      reporter,
		parent:        parent,
		executedTests: make(map[string]*testResult),
	}
}

func (tc *testContext) Name() string {
	return tc.name
}

func (tc *testContext) runTest(fn TestFunc, c *testContext) (err interface{}) {
	defer func() {
		err = recover()
	}()
	fn(c)
	return nil
}

func (tc *testContext) test(run bool, name string, fn TestFunc) bool {
	test, secondPass := tc.executedTests[name]
	if secondPass && !test.HasMore {
		return test.Success
	}

	if !secondPass {
		test = &testResult{Context: newTestContext(name, tc.reporter, tc)}
		tc.executedTests[name] = test
		tc.reporter.TestStart(name)
	}

	if !run {
		test.Success = false
		test.HasMore = false
		tc.reporter.TestSkipped(name)
		return false
	}

	if err := tc.runTest(fn, test.Context); err != nil {
		if u, ok := err.(*testUnwinder); ok {
			test.HasMore = true
			panic(&testUnwinder{HasMore: true, Success: u.Success})
		}

		test.HasMore = false
		test.Success = false

		tc.reporter.TestFailed(name, err)
		panic(&testUnwinder{HasMore: false, Success: false})
	}

	test.HasMore = false
	test.Success = test.Context.success()

	tc.reporter.TestOk(name)
	panic(&testUnwinder{HasMore: true, Success: test.Success})
}

func (tc *testContext) Test(name string, fn TestFunc) bool {
	return tc.test(true, name, fn)
}

func (tc *testContext) TestIf(run bool, name string, fn TestFunc) bool {
	return tc.test(run, name, fn)
}

func (tc *testContext) success() bool {
	for _, test := range tc.executedTests {
		if !test.Success {
			return false
		}
	}
	return true
}

package testlib

import (
	"fmt"
	"io"
	"strings"
)

type StreamReporter struct {
	Writer io.Writer

	level          int
	hasDescendants []bool

	ok      int
	skipped int
	failed  int
}

type DetailedReporter struct {
	Writer io.Writer

	path     []string
	queryLog []string
	ok       int
	skipped  int
	failed   int
}

func (wr *StreamReporter) TestStart(name string) {
	if wr.level > 0 {
		if wr.hasDescendants[wr.level-1] == false {
			fmt.Fprint(wr.Writer, "\n")
		}
		wr.hasDescendants[wr.level-1] = true
	}
	wr.hasDescendants = append(wr.hasDescendants, false)

	indent := strings.Repeat("  ", wr.level)
	fmt.Fprintf(wr.Writer, "%s%s", indent, name)

	wr.level++
}

func (wr *StreamReporter) testResult(name string, result string) {
	if !wr.hasDescendants[wr.level-1] {
		fmt.Fprintf(wr.Writer, " - %s\n", result)
	}
	wr.level--
	wr.hasDescendants = wr.hasDescendants[:wr.level]
}

func (wr *StreamReporter) TestOk(name string) {
	wr.ok++
	wr.testResult(name, "OK")
}

func (wr *StreamReporter) TestSkipped(name string) {
	wr.skipped++
	wr.testResult(name, "SKIPPED")
}

func (wr *StreamReporter) TestFailed(name string, err interface{}) {
	wr.failed++
	wr.testResult(name, fmt.Sprintf("FAILED (%s)", err))
}

func (wr *StreamReporter) BeforeTesting() {
}

func (wr *StreamReporter) AfterTesting() {
	summury := fmt.Sprintf(
		"%d/%d tests passed (%d failed, %d skipped)",
		wr.ok,
		wr.ok+wr.skipped+wr.failed,
		wr.failed,
		wr.skipped,
	)
	fmt.Fprintf(wr.Writer, "\n%s\n%s\n", strings.Repeat("=", len(summury)), summury)
}

func (wr *DetailedReporter) TestStart(name string) {
	wr.path = append(wr.path, name)
}

func (wr *DetailedReporter) TestOk(name string) {
	wr.ok++
	wr.queryLog = nil
	wr.path = wr.path[:len(wr.path)-1]
}

func (wr *DetailedReporter) TestSkipped(name string) {
	wr.skipped++
	wr.queryLog = nil
	wr.path = wr.path[:len(wr.path)-1]
}

func (wr *DetailedReporter) TestFailed(name string, err interface{}) {
	for _, line := range wr.queryLog {
		fmt.Fprint(wr.Writer, line)
	}
	fmt.Fprintf(wr.Writer, "\nTest: %s\nError: %s\n", strings.Join(wr.path, " / "), err)
	wr.failed++
}

func (wr *DetailedReporter) BeforeTesting() {
}

func (wr *DetailedReporter) AfterTesting() {
	summury := fmt.Sprintf(
		"%d/%d tests passed (%d failed, %d skipped)",
		wr.ok,
		wr.ok+wr.skipped+wr.failed,
		wr.failed,
		wr.skipped,
	)
	fmt.Fprintf(wr.Writer, "\n%s\n%s\n", strings.Repeat("=", len(summury)), summury)
}

func (wr *DetailedReporter) log(sid, category, message string) {
	if sid == "" {
		sid = "NO AUTH"
	} else {
		sid = sid[:7]
	}

	logEntry := fmt.Sprintf("[%s] %-16s %s\n", sid, category, message)
	wr.queryLog = append(wr.queryLog, logEntry)
}

func (wr *DetailedReporter) Request(data []byte, sid string) {
	wr.log(sid, "[REQUEST]", string(data))
}

func (wr *DetailedReporter) Response(data []byte, sid string) {
	wr.log(sid, "[RESPONSE]", string(data))
}

func (wr *DetailedReporter) WSConnect(url string, sid string) {
	wr.log(sid, "[WS CONNECT]", url)
}

func (wr *DetailedReporter) WSConnected(sid string) {
	wr.log(sid, "[WS CONNECTED]", "")
}

func (wr *DetailedReporter) WSExpectEvent(event string, sid string) {
	wr.log(sid, "[EXPECT EVENT]", event)
}

func (wr *DetailedReporter) WSGotEvent(data []byte, sid string) {
	wr.log(sid, "[EVENT]", string(data))
}

func (wr *DetailedReporter) WSGotExpectedEvent(data []byte, sid string) {
	wr.log(sid, "[EXPECTED EVENT]", string(data))
}

func (wr *DetailedReporter) WSSendEvent(data []byte, sid string) {
	wr.log(sid, "[SEND EVENT]", string(data))
}

package main

import (
	"flag"
	"fmt"
	"github.com/lanior/mysterious-bomberman/actor"
	"github.com/lanior/mysterious-bomberman/server"
	"github.com/lanior/mysterious-bomberman/tester/testlib"
	"labix.org/v2/mgo"
	"log"
	"net/http"
	"os"
	"runtime"
	"strings"
	"time"
)

type nilWriter struct{}

func (w nilWriter) Write(data []byte) (int, error) {
	return len(data), nil
}

func runServer() {
	server, err := server.New(&server.Config{
		DebugMode:    true,
		WebSocketUrl: "auto",
		DBConfig: &mgo.DialInfo{
			Addrs:    []string{"localhost:27017"},
			Database: "BombStorageTest",
			Timeout:  5 * time.Second,
		},
		Logger: log.New(&nilWriter{}, "", log.Ldate|log.Ltime),
	})

	if err != nil {
		log.Fatal(err)
	}

	http.Handle("/api/", server.Handler())
	go func() {
		log.Fatal(http.ListenAndServe(":3030", nil))
	}()
	runtime.Gosched()
}

var (
	useEmbeddedServer = flag.Bool("embedded", false, "Use embedded server")
	detailed          = flag.Bool("detailed", false, "Show detailed information about first failed test")
	timeout           = flag.Int("timeout", 2000, "WebSocket event expectation timeout (milleseconds)")
)

func main() {
	var url string

	flag.Parse()

	if *useEmbeddedServer {
		runServer()
		url = "http://localhost:3030/api/"
	} else if flag.NArg() >= 1 {
		url = flag.Arg(0)
		if !strings.HasPrefix(url, "http") {
			url = "http://" + url
		}
	} else {
		fmt.Printf("Usage: %s [api-url]\n", os.Args[0])
		flag.PrintDefaults()
		return
	}

	if *timeout < 0 {
		*timeout = 0
	}

	writer := os.Stdout
	if *detailed {
		reporter := &testlib.DetailedReporter{Writer: writer}
		runner := getRunner(actor.New(url, *timeout, reporter))
		runner.Run(reporter, true)
	} else {
		runner := getRunner(actor.New(url, *timeout, nil))
		runner.Run(&testlib.StreamReporter{Writer: writer}, false)
	}
}
